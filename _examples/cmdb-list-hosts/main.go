package main

import (
	"context"
	"fmt"

	"gitlab.oit.duke.edu/devil-ops/enso-wabisabi/pkg/cmdb"
	"gopkg.in/yaml.v2"
)

func main() {
	c, err := cmdb.NewClientWithEnv(nil)
	panicIfErr(err)
	hosts, err := c.Host.List(context.Background(), cmdb.HostListOpts{})
	panicIfErr(err)

	b, err := yaml.Marshal(hosts)
	panicIfErr(err)
	fmt.Println(string(b))
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
