# Enso Wabi-Sabi

[![pipeline status](https://gitlab.oit.duke.edu/devil-ops/enso-wabisabi/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/oit-ssi-systems/enso-wabisabi/-/commits/main)
[![coverage report](https://gitlab.oit.duke.edu/devil-ops/enso-wabisabi/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/oit-ssi-systems/enso-wabisabi/-/commits/main)
 [![Latest Release](https://gitlab.oit.duke.edu/devil-ops/enso-wabisabi/-/badges/release.svg)](https://gitlab.oit.duke.edu/oit-ssi-systems/enso-wabisabi/-/releases)

Interact with Enso API (along with other local service endpoints) for queries, tooling, etc

> Wabi-sabi nurtures all that is authentic by acknowledging three simple realities: nothing lasts, nothing is finished, and nothing is perfect.
>
> -- *Richard Powell*

## Setup

Get a token from [api help page](https://enso-test.oit.duke.edu/home/api_access)

Set env var with:

```bash
export ENSO_TOKEN=token-from-above
```

If you don't have go installed, do so now. Then in the base dir for this repo run 
```bash
go run cmd/enso-wabisabi/main.go
```

## Usage

### Generate AlertManager Rules

```bash
$ enso-wabisabi generate alert-manager-rules
---
groups:
    - name: swap
      rules:
        - alert: Swap Warning
          annotations:
            description: swap size monitoring
            message: 'Swap usage on {{ $labels.host }} is now: {{ $value }}'
            summary: Alerting SwapWarning
          expr: swap_used_percentage{} > 80
          for: 30m
          labels:
            severity: warning
            severitynum: 2
        - alert: Swap Critical
          annotations:
            description: swap size monitoring
            message: 'Swap usage on {{ $labels.host }} is now: {{ $value }}'
            summary: Alerting SwapCritical
          expr: swap_used_percentage{} > 95
          for: 5m
          labels:
            severity: critical
            severitynum: 4
```

Right now there are some hard coded Prometheus defaults in
[default.go](pkg/alertmanager/default.go). These should be moved out to a yaml
file or something somewhere so we don't have to compile every time we wanna
change

## Generate Host targets by VRF

You can generate the host targets file using data from the CMDB currently. This
is done by looking at the support group inside of the CMDB, and then checking
the VRF in Cartographer.

### Examples

Collect all hosts managed by the Systems-UNIX-OIT support group, and label the
job `linux_collect`:

```bash
$ enso-wabisabi generate prometheus-host-targets Systems-UNIX-OIT 'all' --job linux_collect
...
```

Collect unix hosts in the dc-1 and dc-3 VRFs:

```bash
$ enso-wabisabi generate prometheus-host-targets Systems-UNIX-OIT 'dc-1,dc-3' --job linux_collect
...
```

Collect all unix hosts except for what's in `dc-1` or `dc-3`. Using `!` negates
the VRFs from the list. Useful when using `all`:

```bash
$ enso-wabisabi generate prometheus-host-targets Systems-UNIX-OIT 'all,!dc-1,!dc-3' --job linux_collect
...
```

## Generate Network Device Targets

This pulls all the networking devices out of Cartographer, and includes some
labels for each devices based off of Cartographer metadata.

```bash
$ enso-wabisabi generate prometheus-network-targets
...
```

## Authentication

This tool relies on a number of API Authentications to do queries against
various services like Enso and Cartographer. These can be set a number of ways,
either as CLI arguments, ENV Variables or values from yaml files. Pretty much
that anything [viper](https://github.com/spf13/viper) supports.

In addition to the viper style stuff above, you can also use a yaml file that
contains references to where the secrets live in vault using the `--secret-file`
argument. Use this with something like:

```yaml
---
cartographer-user: "ref+vault://foo/bar/cartographer?#/username"
cartographer-token: "ref+vault://foo/bar/cartographer?#/api_key"
cmdb-token: "ref+vault://foo/bar/cmdb?#/token"
```

This format is documented [here](https://github.com/variantdev/vals). And can
use non-vault based secrets as well. Anything specified in this file will take
preference over any env vars or command line argument settings.

### Horizon

VMware Horizon client needs a little authentication configuration. You can do it
the same way as cartographer/cmdb, but use:

```yaml
---
horizon-user: username@win.duke.edu
horizon-pass: the-password
horizon-url: https://some-horizon-server
```
