package alertmanager

import "testing"

func TestDefaults_ExistingKeys(t *testing.T) {
	d, err := Defaults("disk_warning")
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	if d.Alert != "DiskUsage Warning" ||
		d.For != "30m" ||
		d.Annotations.Description != "Alert for Disks getting closer to full, but not there yet" ||
		d.Labels.Severity != "warning" ||
		d.Labels.SeverityNum != 2 {
		t.Fatalf("Expected default values to match, but got: %+v", d)
	}
}

func TestDefaults_NonExistingKeys(t *testing.T) {
	if _, err := Defaults("non_existing_key"); err == nil {
		t.Fatalf("Expected an error, but got none")
	}
}

func TestConfig(t *testing.T) {
	config := Config{
		Groups: []Group{
			{
				Name: "group1",
				Rules: []Rule{
					{
						Alert: "alert1",
						For:   "30m",
					},
				},
			},
		},
	}
	if len(config.Groups) != 1 ||
		len(config.Groups[0].Rules) != 1 ||
		config.Groups[0].Rules[0].Alert != "alert1" ||
		config.Groups[0].Rules[0].For != "30m" {
		t.Fatalf("Expected config to match, but got: %+v", config)
	}
}
