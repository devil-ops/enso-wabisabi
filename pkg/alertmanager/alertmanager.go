/*
Package alertmanager handles all the Prometheus AlertManager pieces
*/
package alertmanager

import (
	"errors"
	"log/slog"
)

// Rule is the alert manager rule
type Rule struct {
	Alert       string         `json:"alert,omitempty"`
	Annotations RuleAnnotation `json:"annotations,omitempty"`
	Expr        string         `json:"expr,omitempty"`
	For         string         `json:"for,omitempty"`
	Labels      RuleLabels     `json:"labels,omitempty"`
}

// RuleAnnotation is the annotation for a rule
type RuleAnnotation struct {
	Description string `json:"description,omitempty"`
	Message     string `json:"message,omitempty"`
	Summary     string `json:"summary,omitempty"`
}

// RuleLabels labels for a given rule
type RuleLabels struct {
	Severity    string `json:"severity,omitempty"`
	SeverityNum int    `json:"severity_num,omitempty"`
}

// Group is the group for an alert
type Group struct {
	Name  string `json:"name,omitempty"`
	Rules []Rule `json:"rules,omitempty"`
}

// Config is the rule configuration
type Config struct {
	Groups []Group `json:"groups,omitempty"`
}

// Define some defaults here to enhance the data we get from Enso
/*
TODREW: This should be moved out to a static config file somewhere instead of
being included in the source
*/
var defaults = map[string]Rule{
	"disk_warning": {
		Alert: "DiskUsage Warning",
		For:   "30m",
		Annotations: RuleAnnotation{
			Description: "Alert for Disks getting closer to full, but not there yet",
			Message:     `The {{ $labels.path }} mount on {{ $labels.host }} is now {{ $value }}% full.`,
			Summary:     "Alerting DiskUsageNearingFull",
		},
		Labels: RuleLabels{
			Severity:    "warning",
			SeverityNum: 2,
		},
	},
	"disk_critical": {
		Alert: "DiskUsage Critical",
		For:   "5m",
		Annotations: RuleAnnotation{
			Description: "Alert for when a disk gets mostly full",
			Message: `The {{ $labels.path }} mount on {{ $labels.host }} is now:
{{ $value }}% full.`,
			Summary: "Alerting DiskUsageFull",
		},
		Labels: RuleLabels{
			Severity:    "critical",
			SeverityNum: 2,
		},
	},
	"swap_crit": {
		Alert: "Swap Critical",
		For:   "5m",
		Annotations: RuleAnnotation{
			Description: "Alert for when a host has high swap usage",
			Message:     `Swap usage on {{ $labels.host }} is now: {{ $value }}`,
			Summary:     "Alerting SwapCritical",
		},
		Expr: `swap_used_percentage{} > THRESH`,
		Labels: RuleLabels{
			Severity:    "critical",
			SeverityNum: 4,
		},
	},
	"swap_warn": {
		Alert: "Swap Warning",
		For:   "30m",
		Expr:  `swap_used_percentage{} > THRESH`,
		Annotations: RuleAnnotation{
			Description: "Alert for when a host has high swap usage",
			Message:     `Swap usage on {{ $labels.host }} is now: {{ $value }}`,
			Summary:     "Alerting SwapWarning",
		},
		Labels: RuleLabels{
			Severity:    "warning",
			SeverityNum: 2,
		},
	},
}

// Defaults returns the default rule for a name
func Defaults(name string) (*Rule, error) {
	if d, ok := defaults[name]; ok {
		return &d, nil
	}
	slog.Warn("no defaults found", "name", name)
	return nil, errors.New("NoDefaultsFound")
}
