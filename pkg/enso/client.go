/*
Package enso interacts with the ENSO API
*/
package enso

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"

	"moul.io/http2curl"
)

const (
	baseURL = "https://enso.oit.duke.edu"
)

// ClientConfig is the configuration for the client
type ClientConfig struct {
	Token string
}

// Response is the response object
type Response struct {
	*http.Response
}

// Client is the base object for interacting with the API
type Client struct {
	client      *http.Client
	UserAgent   string
	Config      ClientConfig
	BaseURL     string
	Monitor     MonitorService
	Maintenance MaintenanceService
	Host        HostService
}

// NewClient returns a new client with some sane defaults
func NewClient(config ClientConfig, httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	userAgent := "enso-wabisabi"
	c := &Client{Config: config, client: httpClient, UserAgent: userAgent, BaseURL: baseURL}
	// c.getToken(config.LongLivedToken)

	c.Monitor = &MonitorServiceOp{client: c}
	c.Maintenance = &MaintenanceServiceOp{client: c}
	c.Host = &HostServiceOp{client: c}
	// c.Location = &LocationServiceOp{client: c}
	// c.Volume = &VolumeServiceOp{client: c}
	return c
}

// The headers are a little bit touchy in this API. This is a wrapper method to send specific headers up to different endpoints
func (c *Client) sendRequestWithHeaders(req *http.Request, v interface{}, headers map[string]string) (*Response, error) {
	for key, val := range headers {
		req.Header.Set(key, val)
	}
	return c.sendRequest(req, v)
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*Response, error) { // nolint:unparam
	// req.Header.Set("Accept", "application/json; charset=utf-8")
	// req.Header.Set("Content-Type", "application/json")
	bearer := "Token " + c.Config.Token
	req.Header.Add("Authorization", bearer)

	command, _ := http2curl.GetCurlCommand(req)
	slog.Debug("curl command", "command", fmt.Sprint(command))

	res, err := c.client.Do(req)
	req.Close = true
	if err != nil {
		return nil, err
	}

	defer dclose(res.Body)

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		switch {
		case res.StatusCode == http.StatusTooManyRequests:
			return nil, fmt.Errorf("too many requests.  Check rate limit and make sure the userAgent is set right")
		case res.StatusCode == http.StatusNotFound:
			return nil, fmt.Errorf("that entry was not found, are you sure it exists?")
		default:
			return nil, fmt.Errorf("error, status code: %d", res.StatusCode)
		}
	}

	b, _ := io.ReadAll(res.Body)
	if string(b) != "" {
		if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
			return nil, err
		}
	} else {
		// When there is no body
		return nil, nil
	}
	r := &Response{res}

	return r, nil
}

// ErrorResponse it the error response from the API
type ErrorResponse struct {
	Message string `json:"errors"`
}

func dclose(c io.Closer) {
	err := c.Close()
	if err != nil {
		slog.Warn("error closing item", "error", err)
	}
}
