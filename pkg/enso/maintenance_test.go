package enso

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMaintByHost(t *testing.T) {
	got, err := c.Maintenance.Start(context.TODO(), "foo.example.edu", time.Minute*5)
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, got.Response, "host foo.example.edu placed into maintenance mode for 0 days, 0 hours and 5 minutes")
}

func TestIsIP(t *testing.T) {
	assert.False(t, isIP("foo.bar"))
	assert.True(t, isIP("10.0.0.1"))
}
