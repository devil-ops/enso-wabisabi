package enso

import (
	"bytes"
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

var (
	srv *httptest.Server
	c   *Client
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func shutdown() {
	srv.Close()
}

func setup() {
	srv = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case strings.HasSuffix(r.URL.Path, "monitor_templates.json"):
			b, err := os.ReadFile("./testdata/monitor_template.json")
			panicIfErr(err)
			_, err = io.Copy(w, bytes.NewReader(b))
			panicIfErr(err)
		case strings.HasSuffix(r.URL.Path, "maintenance/authorized.json"):
			_, err := io.Copy(w, strings.NewReader(`{"response":"host foo.example.edu placed into maintenance mode for 0 days, 0 hours and 5 minutes"}`))
			panicIfErr(err)
		default:
			slog.Warn("unexpected request", "url", r.URL.String())
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	c = NewClient(ClientConfig{}, nil)
	c.BaseURL = srv.URL

	// c = NewClient(conf, nil)
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
