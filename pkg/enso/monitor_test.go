package enso

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListTemplates(t *testing.T) {
	got, _, err := c.Monitor.Templates(context.TODO())
	require.NoError(t, err)
	require.NotNil(t, got)
}
