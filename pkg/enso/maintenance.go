package enso

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"time"
)

// MaintenanceServiceOp is the oeprator for the maint service interface
type MaintenanceServiceOp struct {
	client *Client
}

// MaintenanceService defines how the maint endpoints work
type MaintenanceService interface {
	// StartWithHost(context.Context, HostMaintRequest) (*MaintResponse, error)
	// StartWithIP(context.Context, IPMaintRequest) (*MaintResponse, error)
	Start(context.Context, string, time.Duration) (*MaintResponse, error)
	End(context.Context, string) (*MaintResponse, error)
}

// Start puts a host or IP in maint mod
func (svc *MaintenanceServiceOp) Start(ctx context.Context, target string, duration time.Duration) (*MaintResponse, error) {
	if isIP(target) {
		return svc.changeWithInterface(ctx, http.MethodPost, IPMaintRequest{
			IPAddress: target,
			Minutes:   int(duration.Minutes()),
		}, map[string]string{"Content-Type": "application/json"})
	}
	return svc.changeWithInterface(ctx, http.MethodPost, HostMaintRequest{
		Host:    target,
		Minutes: int(duration.Minutes()),
	}, map[string]string{"Content-Type": "application/json"})
}

// End puts a host or IP in maint mod
func (svc *MaintenanceServiceOp) End(ctx context.Context, target string) (*MaintResponse, error) {
	if isIP(target) {
		return svc.changeWithInterface(ctx, http.MethodDelete, IPMaintRequest{
			IPAddress: target,
		}, map[string]string{"Content-Type": "application/json"})
	}
	return svc.changeWithInterface(ctx, http.MethodDelete, HostMaintRequest{
		Host: target,
	}, map[string]string{"Content-Type": "application/json"})
}

func (svc *MaintenanceServiceOp) changeWithInterface(_ context.Context, action string, r any, headers map[string]string) (*MaintResponse, error) {
	mrj, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest(action, fmt.Sprintf("%s%s", svc.client.BaseURL, "/api/v2/maintenance/authorized.json"), bytes.NewReader(mrj))
	if err != nil {
		return nil, err
	}
	var ret MaintResponse
	// _, err = svc.client.sendRequest(req, &ret)
	_, err = svc.client.sendRequestWithHeaders(req, &ret, headers)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

// HostMaintRequest describes how to request maint mode for a given host
type HostMaintRequest struct {
	Host    string `json:"host"`
	Minutes int    `json:"minutes"`
}

// IPMaintRequest describes how to request maint mode for a given IP
type IPMaintRequest struct {
	IPAddress string `json:"ip_address"`
	Minutes   int    `json:"minutes"`
}

// MaintResponse describes the response back from a maintenance call
type MaintResponse struct {
	Response string `json:"response"`
}

func isIP(s string) bool {
	return net.ParseIP(s) != nil
}
