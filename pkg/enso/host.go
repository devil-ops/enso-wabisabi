package enso

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/netip"
	"strings"
	"time"

	"github.com/spf13/cobra"
)

// IPFilterOpt is a type of IP filtering
type IPFilterOpt int

const (
	// IPFilterAll is the default which is any type of ip
	IPFilterAll IPFilterOpt = iota
	// IPFilterPrivate is just private address space
	IPFilterPrivate
	// IPFilterPublic is just public address space
	IPFilterPublic
)

// HostServiceOp is the operator for a host service
type HostServiceOp struct {
	client *Client
}

// HostService holds on the methods for interacting with the host endpoint
type HostService interface {
	List(ctx *context.Context) ([]HostListEntry, *Response, error)
	Pingable(ctx context.Context) (PingableHostList, *Response, error)
	ICMP(ctx context.Context) (ICMPTargetList, *Response, error)
	// Get(ctx *context.Context, id float64) (Host, *Response, error)
}

// PingableHostList is a list of pingable hosts
type PingableHostList []PingableHost

// PingableHost is a host that can and should be pinged
type PingableHost struct {
	ID         int64      `json:"id,omitempty"`
	Name       string     `json:"name,omitempty"`
	CreatedAt  *time.Time `json:"created_at,omitempty"`
	UpdatedAt  *time.Time `json:"updated_at,omitempty"`
	IPAddress  string     `json:"ip_address,omitempty"`
	DeviceType string     `json:"device_type,omitempty"`
}

// BlackboxTargets returns the host list as targets
func (l PingableHostList) BlackboxTargets() []PromBlackboxTargetConfig {
	ret := make([]PromBlackboxTargetConfig, len(l))
	for idx, device := range l {
		ret[idx] = PromBlackboxTargetConfig{
			Labels: promBlackboxLabels{
				Host:       device.Name,
				DeviceType: device.DeviceType,
			},
			Targets: []string{device.Name},
		}
	}
	return ret
}

// Pingable returns the hosts from the pingable_hosts endpoint
func (svc *HostServiceOp) Pingable(_ context.Context) (PingableHostList, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, "/api/v2/hosts/pingable_hosts"), nil)
	if err != nil {
		return nil, nil, err
	}
	var ret []PingableHost
	_, err = svc.client.sendRequestWithHeaders(req, &ret, map[string]string{"Accept": "application/json; charset=utf-8"})
	if err != nil {
		return nil, nil, err
	}
	return ret, nil, nil
}

// ICMP is a list of items from the icmp_targets endpoint
func (svc *HostServiceOp) ICMP(_ context.Context) (ICMPTargetList, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, "/api/v2/hosts/icmp_targets"), nil)
	if err != nil {
		return nil, nil, err
	}
	var ret ICMPTargetList
	_, err = svc.client.sendRequestWithHeaders(req, &ret, map[string]string{"Accept": "application/json; charset=utf-8"})
	if err != nil {
		return nil, nil, err
	}
	return ret, nil, nil
}

// List lists out host entries
func (svc *HostServiceOp) List(_ *context.Context) ([]HostListEntry, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, "/api/v2/hosts.json"), nil)
	if err != nil {
		return nil, nil, err
	}
	var ret []HostListEntry
	_, err = svc.client.sendRequestWithHeaders(req, &ret, map[string]string{"Accept": "application/json; charset=utf-8"})
	if err != nil {
		return nil, nil, err
	}
	return ret, nil, nil
}

// HostListEntryFilterOpts is just a big ol' filter
type HostListEntryFilterOpts struct {
	NameMatch string
	Monitored *bool
	PrivateIP *bool
	ValidName *bool
	IPs       IPFilterOpt
}

// HostListEntryFilterOptsWithCobra uses a cobra.Command for the filter list
func HostListEntryFilterOptsWithCobra(cmd *cobra.Command, _ []string) (*HostListEntryFilterOpts, error) {
	opts := &HostListEntryFilterOpts{}
	var err error
	opts.NameMatch, err = cmd.Flags().GetString("name")
	if err != nil {
		return nil, err
	}

	if cmd.Flags().Changed("valid-names") {
		vn, verr := cmd.Flags().GetBool("valid-names")
		if verr != nil {
			return nil, verr
		}
		opts.ValidName = &vn
	}

	if cmd.Flags().Changed("only-monitored") {
		_, oerr := cmd.Flags().GetBool("only-monitored")
		if oerr != nil {
			return nil, oerr
		}
		opts.Monitored = boolPTR(true)
	}

	onlyPriv, err := cmd.Flags().GetBool("only-private-ips")
	if err != nil {
		return nil, err
	}
	if onlyPriv {
		opts.IPs = IPFilterPrivate
	}
	onlyPub, err := cmd.Flags().GetBool("only-public-ips")
	if err != nil {
		return nil, err
	}
	if onlyPub {
		opts.IPs = IPFilterPublic
	}
	if onlyPub && onlyPriv {
		return nil, errors.New("do not specify only-public-ips and only-private-ips at the same time")
	}

	return opts, nil
}

// MonitoredBy represents the different types of software something is monitored by
type MonitoredBy struct {
	Akips    bool `json:"akips,omitempty"`
	Icinga   bool `json:"icinga,omitempty"`
	Spectrum bool `json:"spectrum,omitempty"`
}

// HostListEntry is a host list entry from the api
type HostListEntry struct {
	ID          float64     `json:"id,omitempty"`
	CMDBID      *string     `json:"cmdb_id,omitempty"`
	AlarmsURL   string      `json:"alarms_url,omitempty"`
	CreatedAt   string      `json:"created_at,omitempty"`
	DisplayURL  string      `json:"display_url,omitempty"`
	EventsURL   string      `json:"events_url,omitempty"`
	IPAddress   string      `json:"ip_address,omitempty"`
	ModelClass  *string     `json:"model_class,omitempty"`
	MonitoredBy MonitoredBy `json:"monitored_by,omitempty"`
	Name        string      `json:"name,omitempty"`
	UpdatedAt   string      `json:"updated_at,omitempty"`
	URL         string      `json:"url,omitempty"`
}

// Host is a host item from the API
type Host struct {
	ID                     float64       `json:"id,omitempty"`
	AkipsDevice            []interface{} `json:"akips_device,omitempty"`
	AlarmsURL              string        `json:"alarms_url,omitempty"`
	AvailabilityMonitoring bool          `json:"availability_monitoring,omitempty"`
	CreatedAt              string        `json:"created_at,omitempty"`
	DisplayURL             string        `json:"display_url,omitempty"`
	EventsURL              string        `json:"events_url,omitempty"`
	IcingaDevice           []interface{} `json:"icinga_device,omitempty"`
	IcingaServiceChecks    []interface{} `json:"icinga_service_checks,omitempty"`
	InMaintenance          bool          `json:"in_maintenance,omitempty"`
	IPAddress              string        `json:"ip_address,omitempty"`
	MonitorTemplates       []interface{} `json:"monitor_templates,omitempty"`
	MonitoredBy            struct {
		Akips      bool `json:"akips,omitempty"`
		Icinga     bool `json:"icinga,omitempty"`
		Prometheus bool `json:"prometheus,omitempty"`
		Spectrum   bool `json:"spectrum,omitempty"`
	} `json:"monitored_by,omitempty"`
	Name                  string        `json:"name,omitempty"`
	NeobaseID             string        `json:"neobase_id,omitempty"`
	PerformanceMonitoring bool          `json:"performance_monitoring,omitempty"`
	PolicyCompliant       string        `json:"policy_compliant,omitempty"`
	SpectrumDevice        []interface{} `json:"spectrum_device,omitempty"`
	UpdatedAt             string        `json:"updated_at,omitempty"`
	URL                   string        `json:"url,omitempty"`
}

type (
	// HostListEntryFilter filters a single host
	HostListEntryFilter func(HostListEntry, *HostListEntryFilterOpts) bool
	// HostListEntryFilterBulk filters multiple hosts
	HostListEntryFilterBulk func([]HostListEntry, *HostListEntryFilterOpts) []HostListEntry
)

// HostListEntryFilterName filters based on the name
func HostListEntryFilterName(entry HostListEntry, opts *HostListEntryFilterOpts) bool {
	if opts.NameMatch == "" {
		return true
	} else if strings.Contains(entry.Name, opts.NameMatch) {
		return true
	}
	return false
}

// HostListEntryFilterIPs filters based on IP Addresses
func HostListEntryFilterIPs(entry HostListEntry, opts *HostListEntryFilterOpts) bool {
	if opts.IPs == IPFilterAll {
		return true
	}
	if entry.IPAddress == "" && (opts.IPs != IPFilterAll) {
		return false
	}
	switch o := opts.IPs; {
	case o == IPFilterPrivate:
		ip := netip.MustParseAddr(entry.IPAddress)
		return ip.IsPrivate()
	case o == IPFilterPublic:
		ip := netip.MustParseAddr(entry.IPAddress)
		return !ip.IsPrivate()
	default:
		return false
	}
}

// HostListEntryFilterValidName only returns hosts with a valid name (fqdn and such)
func HostListEntryFilterValidName(entry HostListEntry, opts *HostListEntryFilterOpts) bool {
	if opts.ValidName == nil {
		return true
	}
	if *opts.ValidName {
		return strings.Contains(entry.Name, ".")
	}
	return !strings.Contains(entry.Name, ".")
}

// HostListEntryFilterMonitored only returns items monitored with something
func HostListEntryFilterMonitored(entry HostListEntry, opts *HostListEntryFilterOpts) bool {
	// If not set, always return true
	if opts.Monitored == nil {
		return true
	}
	if !*opts.Monitored && (!entry.MonitoredBy.Akips && !entry.MonitoredBy.Spectrum && !entry.MonitoredBy.Icinga) {
		return false
	}
	if *opts.Monitored && (entry.MonitoredBy.Akips || entry.MonitoredBy.Icinga || entry.MonitoredBy.Spectrum) {
		return true
	}
	return false
}

// ApplyHostListEntryFiltersWithOpts applies some default filters to a set of records
func ApplyHostListEntryFiltersWithOpts(records []HostListEntry, opts *HostListEntryFilterOpts) []HostListEntry {
	return ApplyHostListEntryFilters(
		records,
		opts,
		HostListEntryFilterMonitored,
		HostListEntryFilterName,
		HostListEntryFilterValidName,
		HostListEntryFilterIPs,
	)
}

// ApplyHostListEntryFilters applies given filters
func ApplyHostListEntryFilters(records []HostListEntry, opts *HostListEntryFilterOpts, filters ...HostListEntryFilter) []HostListEntry {
	if len(filters) == 0 {
		return records
	}
	filteredRecords := make([]HostListEntry, 0, len(records))

	for _, r := range records {
		keep := true

		for _, f := range filters {
			if !f(r, opts) {
				keep = false
				break
			}
		}

		if keep {
			filteredRecords = append(filteredRecords, r)
		}
	}

	return filteredRecords
}

func boolPTR(b bool) *bool {
	return &b
}

// Targets returns a list of PromBlackboxTargetConfig objects
func (l PingableHostList) Targets() []PromBlackboxTargetConfig {
	promConfig := make([]PromBlackboxTargetConfig, len(l))
	for idx, device := range l {
		promConfig[idx] = PromBlackboxTargetConfig{
			Labels: promBlackboxLabels{
				Host:       device.Name,
				DeviceType: device.DeviceType,
			},
			Targets: []string{device.Name},
		}
	}
	return promConfig
}

// PromBlackboxTargetConfig is an item of blackbox configuration
type PromBlackboxTargetConfig struct {
	Labels  promBlackboxLabels `json:"labels,omitempty"`
	Targets []string           `json:"targets,omitempty"`
}

type promBlackboxLabels struct {
	Host       string `json:"host,omitempty"`
	DeviceType string `json:"device_type,omitempty"`
}

// ICMPTargetList is multiple ICMPTargset items
type ICMPTargetList []ICMPTarget

// ICMPTarget is an item from the icmp_target endpoint
type ICMPTarget struct {
	Applications      []string `json:"applications,omitempty"`
	CustomerGroup     string   `json:"customer_group,omitempty"`
	DBSupportGroup    string   `json:"db_support_group,omitempty"`
	DeviceCriticality string   `json:"device_criticality,omitempty"`
	DeviceType        string   `json:"device_type,omitempty"`
	FQDN              string   `json:"fqdn,omitempty"`
	ID                int      `json:"id,omitempty"`
	IPAddress         string   `json:"ip_address,omitempty"`
	NoDelay           bool     `json:"no_delay,omitempty"`
	OsSupportGroup    string   `json:"os_support_group,omitempty"`
}

// Targets returns a list of PromBlackboxTargetConfig objects
func (l ICMPTargetList) Targets() []PromBlackboxTargetConfig {
	promConfig := make([]PromBlackboxTargetConfig, len(l))
	for idx, device := range l {
		promConfig[idx] = PromBlackboxTargetConfig{
			Labels: promBlackboxLabels{
				Host:       device.FQDN,
				DeviceType: "VM",
			},
			Targets: []string{device.FQDN},
		}
	}
	return promConfig
}
