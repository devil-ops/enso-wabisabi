package enso

import (
	"context"
	"fmt"
	"net/http"
)

// MonitorServiceOp is the operator for the MonitorService
type MonitorServiceOp struct {
	client *Client
}

// MonitorService holds the methods for the monitoring endpoint
type MonitorService interface {
	Templates(ctx context.Context) ([]MonitorTemplate, *Response, error)
}

// Templates returns the available monitoring templates
func (svc *MonitorServiceOp) Templates(_ context.Context) ([]MonitorTemplate, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, "/api/v2/monitor_templates.json"), nil)
	if err != nil {
		return nil, nil, err
	}
	var ret []MonitorTemplate
	_, err = svc.client.sendRequestWithHeaders(req, &ret, map[string]string{"Accept": "application/json; charset=utf-8"})
	if err != nil {
		return nil, nil, err
	}
	return ret, nil, nil
}

// MonitorTemplate is the object representing a Monitoring Template
type MonitorTemplate struct {
	CreatedAt              string                  `json:"created_at,omitempty"`
	Description            string                  `json:"description,omitempty"`
	DisplayURL             string                  `json:"display_url,omitempty"`
	IcingaHostTemplate     *string                 `json:"icinga_host_template,omitempty"`
	IcingaServiceTemplates []IcingaServiceTemplate `json:"icinga_service_templates,omitempty"`
	ID                     float64                 `json:"id,omitempty"`
	Name                   string                  `json:"name,omitempty"`
	SpectrumHostTemplate   string                  `json:"spectrum_host_template,omitempty"`
	UpdatedAt              string                  `json:"updated_at,omitempty"`
	URL                    string                  `json:"url,omitempty"`
	Metrics                []Metric                `json:"metrics,omitempty"`
}

// IcingaServiceTemplate are the pieces needed for an Icinga template
type IcingaServiceTemplate struct {
	Name         string `json:"name,omitempty"`
	DisplayName  string `json:"display_name,omitempty"`
	CheckCommand string `json:"check_command,omitempty"`
}

// Metric is a single metric
type Metric struct {
	CreatedAt     string  `json:"created_at,omitempty"`
	Description   string  `json:"description,omitempty"`
	MetricType    string  `json:"metric_type,omitempty"`
	Name          string  `json:"name,omitempty"`
	OsFamily      string  `json:"os_family,omitempty"`
	ThresholdType string  `json:"threshold_type,omitempty"`
	ThresholdCrit *int    `json:"threshold_crit,omitempty"`
	ThresholdWarn *int    `json:"threshold_warn,omitempty"`
	Timeout       float64 `json:"timeout,omitempty"`
	UpdatedAt     string  `json:"updated_at,omitempty"`
}
