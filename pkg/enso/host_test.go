package enso

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestHostListEntryFilterName(t *testing.T) {
	tests := map[string]struct {
		host HostListEntry
		opts HostListEntryFilterOpts
		want bool
	}{
		"always-match": {
			host: HostListEntry{
				Name: "foo.com",
			},
			opts: HostListEntryFilterOpts{},
			want: true,
		},
		"match-specified": {
			host: HostListEntry{
				Name: "foo.com",
			},
			opts: HostListEntryFilterOpts{
				NameMatch: "o.c",
			},
			want: true,
		},
		"no-match-specified": {
			host: HostListEntry{
				Name: "foo.com",
			},
			opts: HostListEntryFilterOpts{
				NameMatch: "bar",
			},
			want: false,
		},
	}

	for desc, tt := range tests {
		got := HostListEntryFilterName(tt.host, &tt.opts)
		require.Equal(t, tt.want, got, desc)
	}
}

func TestHostListEntryFilterMonitored(t *testing.T) {
	tests := map[string]struct {
		host HostListEntry
		opts HostListEntryFilterOpts
		want bool
	}{
		"always-match": {
			host: HostListEntry{
				Name: "foo.com",
			},
			opts: HostListEntryFilterOpts{},
			want: true,
		},
		"match-specified": {
			host: HostListEntry{
				Name: "foo.com",
				MonitoredBy: MonitoredBy{
					Spectrum: true,
				},
			},
			opts: HostListEntryFilterOpts{
				Monitored: boolPTR(true),
			},
			want: true,
		},
		"not-match-specified": {
			host: HostListEntry{
				Name:        "foo.com",
				MonitoredBy: MonitoredBy{},
			},
			opts: HostListEntryFilterOpts{
				Monitored: boolPTR(false),
			},
			want: false,
		},
	}

	for desc, tt := range tests {
		got := HostListEntryFilterMonitored(tt.host, &tt.opts)
		require.Equal(t, tt.want, got, desc)
	}
}

func TestHostListEntryFilterIPs(t *testing.T) {
	tests := map[string]struct {
		host HostListEntry
		opts HostListEntryFilterOpts
		want bool
	}{
		"empty-opt": {
			host: HostListEntry{
				IPAddress: "10.1.2.3",
			},
			opts: HostListEntryFilterOpts{},
			want: true,
		},
		"empty-ip": {
			host: HostListEntry{
				IPAddress: "",
			},
			opts: HostListEntryFilterOpts{
				IPs: IPFilterAll,
			},
			want: true,
		},
		"empty-ip-with-private": {
			host: HostListEntry{
				IPAddress: "",
			},
			opts: HostListEntryFilterOpts{
				IPs: IPFilterPrivate,
			},
			want: false,
		},
		"private": {
			host: HostListEntry{
				IPAddress: "10.1.2.3",
			},
			opts: HostListEntryFilterOpts{
				IPs: IPFilterPrivate,
			},
			want: true,
		},
		"private-miss": {
			host: HostListEntry{
				IPAddress: "8.8.8.8",
			},
			opts: HostListEntryFilterOpts{
				IPs: IPFilterPrivate,
			},
			want: false,
		},
		"public": {
			host: HostListEntry{
				IPAddress: "8.8.8.8",
			},
			opts: HostListEntryFilterOpts{
				IPs: IPFilterPublic,
			},
			want: true,
		},
		"public-miss": {
			host: HostListEntry{
				IPAddress: "10.1.2.3",
			},
			opts: HostListEntryFilterOpts{
				IPs: IPFilterPublic,
			},
			want: false,
		},
	}
	for desc, tt := range tests {
		got := HostListEntryFilterIPs(tt.host, &tt.opts)
		require.Equal(t, tt.want, got, desc)
	}
}

func TestHostListEntryFilterValidName(t *testing.T) {
	tests := map[string]struct {
		host HostListEntry
		opts HostListEntryFilterOpts
		want bool
	}{
		"undefined": {
			host: HostListEntry{
				Name: "foo",
			},
			opts: HostListEntryFilterOpts{},
			want: true,
		},
		"valid": {
			host: HostListEntry{
				Name: "foo.bar.com",
			},
			opts: HostListEntryFilterOpts{
				ValidName: boolPTR(true),
			},
			want: true,
		},
		"invalid": {
			host: HostListEntry{
				Name: "foo",
			},
			opts: HostListEntryFilterOpts{
				ValidName: boolPTR(true),
			},
			want: false,
		},
	}
	for desc, tt := range tests {
		got := HostListEntryFilterValidName(tt.host, &tt.opts)
		require.Equal(t, tt.want, got, desc)
	}
}

func TestFilterHostListEntriesWithOpts(t *testing.T) {
	got := ApplyHostListEntryFiltersWithOpts([]HostListEntry{
		{
			Name: "foo",
		},
	}, &HostListEntryFilterOpts{
		NameMatch: "bar",
	})
	require.Equal(t, 0, len(got))
}

func TestApplyHostListEntryFilters(t *testing.T) {
	// Filter out
	got := ApplyHostListEntryFilters([]HostListEntry{
		{
			Name: "foo",
		},
	}, &HostListEntryFilterOpts{
		NameMatch: "bar",
	}, HostListEntryFilterName)
	require.Equal(t, 0, len(got))

	// Filter in
	got = ApplyHostListEntryFilters([]HostListEntry{
		{
			Name: "foo",
		},
	}, &HostListEntryFilterOpts{
		NameMatch: "foo",
	}, HostListEntryFilterName)
	require.Equal(t, 1, len(got))

	// No filter (hashtag)
	got = ApplyHostListEntryFilters([]HostListEntry{
		{
			Name: "foo",
		},
	}, &HostListEntryFilterOpts{})
	require.Equal(t, 1, len(got))
}

func TestPromBlackboxTargets(t *testing.T) {
	require.Equal(t,
		[]PromBlackboxTargetConfig{
			{
				Labels:  promBlackboxLabels{Host: "foo", DeviceType: "switch"},
				Targets: []string{"foo"},
			},
		}, PingableHostList{
			{
				Name:       "foo",
				DeviceType: "switch",
			},
		}.Targets())
}
