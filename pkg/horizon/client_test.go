package horizon

import (
	"bytes"
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

var (
	srv *httptest.Server
	c   *Client
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func shutdown() {
	srv.Close()
}

func setup() {
	rdsCount := 0
	srv = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case strings.HasSuffix(r.URL.Path, "/rest/login"):
			_, err := io.Copy(w, bytes.NewReader([]byte(`{ "access_token": "good-access-token", "refresh_token": "good-refresh-token" }`)))
			panicIfErr(err)
		case strings.HasSuffix(r.URL.Path, "v1/rds-servers"):
			if rdsCount > 0 {
				w.WriteHeader(http.StatusNotFound)
				return
			}
			rdsCount++
			b, err := os.ReadFile("./testdata/rds-servers.json")
			panicIfErr(err)
			_, err = io.Copy(w, bytes.NewReader(b))
			panicIfErr(err)
			// log.Warn().Int("count", rdsCount).Send()
		default:
			slog.Warn("unexpected request", "url", r.URL.String())
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	// c = NewClient(conf, nil)
	c = New(
		WithUsername("foo"),
		WithPassword("bar"),
		WithDomain("baz"),
		WithBaseURL(srv.URL),
	)
}

func TestDoAuth(t *testing.T) {
	srv = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		_, err := io.Copy(w, bytes.NewReader([]byte(`{ "access_token": "good-access-token", "refresh_token": "good-refresh-token" }`)))
		panicIfErr(err)
	}))
	conf := &ClientAuth{
		Username: "foo",
		Password: "bar",
		Domain:   "baz",
	}

	got, err := doAuth(conf, srv.URL)
	require.NoError(t, err)
	require.NotNil(t, got)
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

func TestCobraViper(t *testing.T) {
	cmd := &cobra.Command{}
	BindCobra(cmd)
	cmd.Execute()
	err := BindViperWithCobra(viper.GetViper(), cmd)
	require.NoError(t, err)
}

func TestExpandUser(t *testing.T) {
	tests := map[string]struct {
		given      string
		wantUser   string
		wantDomain string
	}{
		"with-domain": {
			given:      "foo@bar.com",
			wantUser:   "foo",
			wantDomain: "bar.com",
		},
		"no-domain": {
			given:      "foo",
			wantUser:   "foo",
			wantDomain: "",
		},
	}
	for desc, tt := range tests {
		gotu, gotd := expandUser(tt.given)
		require.Equal(t, tt.wantUser, gotu, desc)
		require.Equal(t, tt.wantDomain, gotd, desc)
	}
}
