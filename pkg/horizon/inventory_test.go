package horizon

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetRDSServers(t *testing.T) {
	got, err := c.Inventory.RDSServers()
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Greater(t, len(got), 0)
}
