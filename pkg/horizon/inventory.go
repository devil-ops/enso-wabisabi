package horizon

import (
	"fmt"
	"log/slog"
	"net/http"
)

// InventoryService holds methods for interacting with the Inventory endpoint
type InventoryService interface {
	RDSServers() ([]RDSServer, error)
	Farms() ([]Farm, error)
	Farm(string) (*Farm, error)
}

// InventoryServiceOp is the operator for the InventoryService
type InventoryServiceOp struct {
	client *Client
}

// RDSServer represents the remote desktop server from the inventory api
type RDSServer struct {
	AccessGroupID                        string `json:"access_group_id,omitempty"`
	AgentBuildNumber                     string `json:"agent_build_number,omitempty"`
	AgentVersion                         string `json:"agent_version,omitempty"`
	BaseVMID                             string `json:"base_vm_id,omitempty"`
	BaseVMSnapshotID                     string `json:"base_vm_snapshot_id,omitempty"`
	Description                          string `json:"description,omitempty"`
	DNSName                              string `json:"dns_name,omitempty"`
	Enabled                              bool   `json:"enabled,omitempty"`
	FarmID                               string `json:"farm_id,omitempty"`
	ID                                   string `json:"id,omitempty"`
	LoadIndex                            int    `json:"load_index,omitempty"`
	LoadPreference                       string `json:"load_preference,omitempty"`
	MaxSessionsCount                     int    `json:"max_sessions_count,omitempty"`
	MaxSessionsCountConfigured           int    `json:"max_sessions_count_configured,omitempty"`
	MaxSessionsType                      string `json:"max_sessions_type,omitempty"`
	MaxSessionsTypeConfigured            string `json:"max_sessions_type_configured,omitempty"`
	MessageSecurityEnhancedModeSupported bool   `json:"message_security_enhanced_mode_supported,omitempty"`
	MessageSecurityMode                  string `json:"message_security_mode,omitempty"`
	Name                                 string `json:"name,omitempty"`
	OperatingSystem                      string `json:"operating_system,omitempty"`
	OperationState                       string `json:"operation_state,omitempty"`
	RemoteExperienceAgentBuildNumber     string `json:"remote_experience_agent_build_number,omitempty"`
	SessionCount                         int    `json:"session_count,omitempty"`
	State                                string `json:"state,omitempty"`
}

// Farms probes the farms endpoint
func (svc *InventoryServiceOp) Farms() ([]Farm, error) { // nolint:dupl // I don't know how to dedup this
	curPage := 1
	perPage := 10
	var ret []Farm
	for {
		slog.Debug("page", "current", curPage, "perPage", perPage)
		req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, fmt.Sprintf("/rest/inventory/v4/farms?page=%v&size=%v", curPage, perPage)), nil)
		if err != nil {
			return nil, err
		}

		var pageItems []Farm
		err = svc.client.sendRequest(req, &pageItems)
		if (err != nil) || (len(pageItems) == 0) {
			slog.Debug("error encountered", "error", err)
			break
		}
		ret = append(ret, pageItems...)
		curPage++
	}
	return ret, nil
}

// RDSServers probes the rds-server endpoint inside of the inventory service
func (svc *InventoryServiceOp) RDSServers() ([]RDSServer, error) { // nolint:dupl // I don't know how to dedup this
	curPage := 1
	perPage := 10
	var ret []RDSServer
	for {
		slog.Debug("pages", "current", curPage, "perPage", perPage)
		req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, fmt.Sprintf("/rest/inventory/v1/rds-servers?page=%v&size=%v", curPage, perPage)), nil)
		if err != nil {
			return nil, err
		}

		var pageItems []RDSServer
		err = svc.client.sendRequest(req, &pageItems)
		if (err != nil) || (len(pageItems) == 0) {
			slog.Debug("error encountered", "error", err)
			break
		}
		ret = append(ret, pageItems...)
		curPage++
	}
	return ret, nil
}

// Farm returns a specific farm with a given id
func (svc *InventoryServiceOp) Farm(id string) (*Farm, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, fmt.Sprintf("/rest/inventory/v3/farms/%v", id)), nil)
	if err != nil {
		return nil, err
	}

	var ret Farm
	err = svc.client.sendRequest(req, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

// Farm Represents a Horizon Farm
type Farm struct {
	AccessGroupID         string `json:"access_group_id,omitempty"`
	AutomatedFarmSettings struct {
		CustomizationSettings struct {
			AdContainerRdn                 string   `json:"ad_container_rdn,omitempty"`
			CloneprepCustomizationSettings struct{} `json:"cloneprep_customization_settings,omitempty"`
			CustomizationType              string   `json:"customization_type,omitempty"`
			InstantCloneDomainAccountID    string   `json:"instant_clone_domain_account_id,omitempty"`
			ReusePreExistingAccounts       bool     `json:"reuse_pre_existing_accounts,omitempty"`
		} `json:"customization_settings,omitempty"`
		EnableProvisioning          bool    `json:"enable_provisioning,omitempty"`
		ImageSource                 string  `json:"image_source,omitempty"`
		MaxSessionType              string  `json:"max_session_type,omitempty"`
		MinReadyVms                 float64 `json:"min_ready_vms,omitempty"`
		OperatingSystem             string  `json:"operating_system,omitempty"`
		OperatingSystemArchitecture string  `json:"operating_system_architecture,omitempty"`
		PatternNamingSettings       struct {
			MaxNumberOfRdsServers float64 `json:"max_number_of_rds_servers,omitempty"`
			NamingPattern         string  `json:"naming_pattern,omitempty"`
		} `json:"pattern_naming_settings,omitempty"`
		ProvisioningSettings struct {
			BaseSnapshotID                  string  `json:"base_snapshot_id,omitempty"`
			ComputeProfileNumCoresPerSocket float64 `json:"compute_profile_num_cores_per_socket,omitempty"`
			ComputeProfileNumCPUs           float64 `json:"compute_profile_num_cpus,omitempty"`
			ComputeProfileRAMMB             float64 `json:"compute_profile_ram_mb,omitempty"`
			DatacenterID                    string  `json:"datacenter_id,omitempty"`
			HostOrClusterID                 string  `json:"host_or_cluster_id,omitempty"`
			ParentVMID                      string  `json:"parent_vm_id,omitempty"`
			ResourcePoolID                  string  `json:"resource_pool_id,omitempty"`
			VMFolderID                      string  `json:"vm_folder_id,omitempty"`
		} `json:"provisioning_settings,omitempty"`
		ProvisioningStatusData struct {
			InstantCloneCurrentImageState string `json:"instant_clone_current_image_state,omitempty"`
			InstantCloneOperation         string `json:"instant_clone_operation,omitempty"`
		} `json:"provisioning_status_data,omitempty"`
		StopProvisioningOnError bool `json:"stop_provisioning_on_error,omitempty"`
		StorageSettings         struct {
			Datastores []struct {
				DatastoreID       string `json:"datastore_id,omitempty"`
				StorageOvercommit string `json:"storage_overcommit,omitempty"`
			} `json:"datastores,omitempty"`
			UseSeparateDatastoresReplicaAndOsDisks bool `json:"use_separate_datastores_replica_and_os_disks,omitempty"`
			UseViewStorageAccelerator              bool `json:"use_view_storage_accelerator,omitempty"`
			UseVsan                                bool `json:"use_vsan,omitempty"`
		} `json:"storage_settings,omitempty"`
		TransparentPageSharingScope string `json:"transparent_page_sharing_scope,omitempty"`
		VcenterID                   string `json:"vcenter_id,omitempty"`
	} `json:"automated_farm_settings,omitempty"`
	DeleteInProgress        bool   `json:"delete_in_progress,omitempty"`
	DisplayName             string `json:"display_name,omitempty"`
	DisplayProtocolSettings struct {
		AllowUsersToChooseProtocol  bool   `json:"allow_users_to_choose_protocol,omitempty"`
		DefaultDisplayProtocol      string `json:"default_display_protocol,omitempty"`
		GridVgpusEnabled            bool   `json:"grid_vgpus_enabled,omitempty"`
		SessionCollaborationEnabled bool   `json:"session_collaboration_enabled,omitempty"`
		VgpuGridProfile             string `json:"vgpu_grid_profile,omitempty"`
	} `json:"display_protocol_settings,omitempty"`
	Enabled              bool   `json:"enabled,omitempty"`
	ID                   string `json:"id,omitempty"`
	LoadBalancerSettings struct {
		ConnectingSessionThreshold float64 `json:"connecting_session_threshold,omitempty"`
		CPUThreshold               float64 `json:"cpu_threshold,omitempty"`
		DiskQueueLengthThreshold   float64 `json:"disk_queue_length_threshold,omitempty"`
		DiskReadLatencyThreshold   float64 `json:"disk_read_latency_threshold,omitempty"`
		DiskWriteLatencyThreshold  float64 `json:"disk_write_latency_threshold,omitempty"`
		IncludeSessionCount        bool    `json:"include_session_count,omitempty"`
		LoadIndexThreshold         float64 `json:"load_index_threshold,omitempty"`
		MemoryThreshold            float64 `json:"memory_threshold,omitempty"`
	} `json:"load_balancer_settings,omitempty"`
	Name                 string  `json:"name,omitempty"`
	ServerErrorThreshold float64 `json:"server_error_threshold,omitempty"`
	SessionSettings      struct {
		DisconnectedSessionTimeoutPolicy string  `json:"disconnected_session_timeout_policy,omitempty"`
		EmptySessionTimeoutMinutes       float64 `json:"empty_session_timeout_minutes,omitempty"`
		EmptySessionTimeoutPolicy        string  `json:"empty_session_timeout_policy,omitempty"`
		LogoffAfterTimeout               bool    `json:"logoff_after_timeout,omitempty"`
		PreLaunchSessionTimeoutMinutes   float64 `json:"pre_launch_session_timeout_minutes,omitempty"`
		PreLaunchSessionTimeoutPolicy    string  `json:"pre_launch_session_timeout_policy,omitempty"`
		SessionTimeoutPolicy             string  `json:"session_timeout_policy,omitempty"`
	} `json:"session_settings,omitempty"`
	Type                            string `json:"type,omitempty"`
	UseCustomScriptForLoadBalancing bool   `json:"use_custom_script_for_load_balancing,omitempty"`
}
