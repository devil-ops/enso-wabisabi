/*
Package horizon interacts with the VMware Horizon service. This may need to be
moved to it's own module at some point
*/
package horizon

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/url"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/vmware/govmomi"
	"moul.io/http2curl"
)

const (
	baseURL   = "https://oit-hrzcs-tap1.oit.duke.edu"
	domain    = "WIN.DUKE.EDU"
	userAgent = "enso-wabisabi"
)

// New uses functional options to generate a new client
func New(opts ...func(*Client)) *Client {
	c := &Client{
		client:    http.DefaultClient,
		BaseURL:   baseURL,
		UserAgent: userAgent,
	}
	for _, o := range opts {
		o(c)
	}

	c.Inventory = &InventoryServiceOp{client: c}
	return c
}

// NewVMWareClientWithViper returns a govmomi client from viper credentials.
// This is a hack to get some additional info out of vSphere that isn't
// currently presented through the Horizon API
func NewVMWareClientWithViper(v *viper.Viper) (*govmomi.Client, error) {
	user := v.GetString("vcenter-user")
	pass := v.GetString("vcenter-pass")
	burl := v.GetString("vcenter-url")

	if user == "" {
		return nil, errors.New("must set vcenter-user")
	}
	if pass == "" {
		return nil, errors.New("must set vcenter-pass")
	}
	if burl == "" {
		return nil, errors.New("must set vcenter-url")
	}
	// strip off /sdk if the user put it in here, we'll add it where needed
	burl = strings.TrimSuffix(burl, "/sdk")
	vsURL, err := url.Parse(fmt.Sprintf("%v/sdk", burl))
	if err != nil {
		return nil, err
	}

	vs, err := govmomi.NewClient(context.TODO(), vsURL, true)
	if err != nil {
		return nil, err
	}

	err = vs.Login(context.TODO(), url.UserPassword(user, pass))
	if err != nil {
		return nil, err
	}
	return vs, nil
}

// NewWithViper returns a new client using authentication information from Viper
func NewWithViper(v *viper.Viper) (*Client, error) {
	c := New(
		WithUsername(v.GetString("horizon-user")),
		WithPassword(v.GetString("horizon-pass")),
		WithDomain(v.GetString("horizon-domain")),
		WithBaseURL(v.GetString("horizon-url")),
	)

	if c.Auth.Username == "" {
		return nil, errors.New("must set a horizon-user")
	}
	if c.BaseURL == "" {
		return nil, errors.New("must set a horizon-url")
	}

	if c.Auth.Password == "" {
		return nil, errors.New("must set a horizon-pass")
	}

	if c.Auth.Domain == "" {
		return nil, errors.New("must set a horizon-domain")
	}

	return c, nil
}

// WithUsername sets the auth username
func WithUsername(s string) func(*Client) {
	username, domain := expandUser(s)
	return func(c *Client) {
		if username != "" {
			c.Auth.Username = username
		}
		if domain != "" {
			c.Auth.Domain = domain
		}
	}
}

// WithPrintCurl sets printCurl to true
func WithPrintCurl() func(*Client) {
	return func(c *Client) {
		c.printCurl = true
	}
}

// WithPassword sets the auth password
func WithPassword(s string) func(*Client) {
	return func(c *Client) {
		if s != "" {
			c.Auth.Password = s
		}
	}
}

// WithDomain sets the authentication AD domain
func WithDomain(s string) func(*Client) {
	return func(c *Client) {
		if s != "" {
			c.Auth.Domain = s
		}
	}
}

// WithBaseURL sets the base url for all API calls
func WithBaseURL(s string) func(*Client) {
	return func(c *Client) {
		c.BaseURL = s
	}
}

// ClientAuth is the json blob for getting a bearer token
type ClientAuth struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Domain   string `json:"domain"`
}

// ClientTokens holds the access and refresh tokens for the API
type ClientTokens struct {
	AccessToken  string `json:"access_token,omitempty"`
	RefreshToken string `json:"refresh_token,omitempty"`
}

// Response returns the http infor in a response along with any other needed data
type Response struct {
	*http.Response
}

// Client is the thing that interacts with the API
type Client struct {
	client    *http.Client
	UserAgent string
	// Config    ClientConfig
	BaseURL   string
	Inventory InventoryService
	Auth      ClientAuth
	token     string
	printCurl bool
}

func (c *Client) sendRequest(req *http.Request, v interface{}) error {
	if c.token == "" {
		tokens, err := doAuth(&c.Auth, c.BaseURL)
		if err != nil {
			return err
		}
		c.token = tokens.AccessToken
	}
	req.Header.Add("Authorization", "Bearer "+c.token)

	if c.printCurl {
		command, _ := http2curl.GetCurlCommand(req)
		slog.Info("curl command", "command", fmt.Sprint(command))
	}

	res, err := c.client.Do(req)
	req.Close = true
	if err != nil {
		return err
	}

	defer dclose(res.Body)

	err = checkIfAPIErr(res)
	if err != nil {
		return err
	}

	b, _ := io.ReadAll(res.Body)
	if string(b) != "" {
		if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
			slog.Warn("invalid json returned", "body", string(b))
			return err
		}
	} else {
		// When there is no body
		return nil
	}

	return nil
}

func doAuth(c *ClientAuth, baseURL string) (*ClientTokens, error) {
	authS, err := json.Marshal(c)
	if err != nil {
		return nil, err
	}
	/*
		req, rerr := http.NewRequest("POST", fmt.Sprintf("%v/rest/login", baseURL), bytes.NewReader(authS))
		if rerr != nil {
			return nil, rerr
		}
	*/

	res, err := http.Post(fmt.Sprintf("%v/rest/login", baseURL), "application/json", bytes.NewReader(authS))
	if err != nil {
		return nil, err
	}

	defer dclose(res.Body)
	var tokens ClientTokens

	if cerr := checkIfAPIErr(res); cerr != nil {
		return nil, cerr
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	jerr := json.Unmarshal(b, &tokens)
	if jerr != nil {
		return nil, jerr
	}
	return &tokens, nil
}

// ErrorResponse is everything needed from an Error
type ErrorResponse struct {
	Errors    []Error `json:"errors,omitempty"`
	Status    string  `json:"status,omitempty"`
	Timestamp float64 `json:"timestamp,omitempty"`
}

// Error is a thing that can get returned in an error response
type Error struct {
	ErrorKey     string `json:"error_key,omitempty"`
	ErrorMessage string `json:"error_message,omitempty"`
}

func dclose(c io.Closer) {
	err := c.Close()
	if err != nil {
		slog.Warn("error closing item", "error", err)
	}
}

// BindCobra enables the cli options horizon needs to authenticate
func BindCobra(cmd *cobra.Command) {
	cmd.PersistentFlags().String("horizon-user", "", "User to use for Horizon authentication")
	cmd.PersistentFlags().String("horizon-pass", "", "Password to use for Horizon authentication")
	cmd.PersistentFlags().String("horizon-domain", "win.duke.edu", "Domain to use for Horizon authentication")
	cmd.PersistentFlags().String("horizon-url", "https://oit-hrzcs-tap1.oit.duke.edu", "Base URL to use for Horizon authentication")
	cmd.PersistentFlags().String("vcenter-user", "", "User to use for vCenter authentication")
	cmd.PersistentFlags().String("vcenter-pass", "", "Password to use for vCenter authentication")
	cmd.PersistentFlags().String("vcenter-url", "", "Base URL to use for vCenter authentication")
}

// BindViperWithCobra binds a set of Cobra flags to viper
func BindViperWithCobra(v *viper.Viper, cmd *cobra.Command) error {
	for _, item := range []string{"horizon-user", "horizon-pass", "horizon-domain", "horizon-url"} {
		err := v.BindPFlag(item, cmd.Flags().Lookup(item))
		if err != nil {
			return err
		}
	}
	return nil
}

func checkIfAPIErr(res *http.Response) error {
	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		if eerr := json.NewDecoder(res.Body).Decode(&errRes); eerr == nil {
			for _, e := range errRes.Errors {
				slog.Warn("error", "key", e.ErrorKey, "msg", e.ErrorMessage)
			}
			return errors.New(res.Status)
		}
	}
	return nil
}

// Return the username and domain from a username with an @
func expandUser(u string) (string, string) {
	if strings.Contains(u, "@") {
		parts := strings.Split(u, "@")
		return parts[0], parts[1]
	}
	return u, ""
}
