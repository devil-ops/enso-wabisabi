package cmdb

import (
	"bytes"
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

var (
	srv *httptest.Server
	c   *Client
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func shutdown() {
	srv.Close()
}

func setup() {
	srv = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, "/api/v1/servers.json") {
			rp, err := os.ReadFile("../testdata/cmdb/servers.json")
			if err != nil {
				panic(err)
			}
			_, err = io.Copy(w, bytes.NewReader(rp))
			if err != nil {
				panic(err)
			}
			return
		}
		slog.Warn("unexpected request", "url", r.URL.String())
		w.WriteHeader(http.StatusNotFound)
	}))
	c = NewClient(ClientConfig{}, nil)
	c.BaseURL = srv.URL
}

func TestNew(t *testing.T) {
	require.NotNil(t, New(
		WithToken("some-token"),
	))
}

func TestNewWithEnv(t *testing.T) {
	os.Clearenv()
	_, err := NewClientWithEnv()
	require.EqualError(t, err, "must set CMDB_TOKEN when using NewClientWithEnv()")
	t.Setenv("CMDB_TOKEN", "some-token")
	_, err = NewClientWithEnv()
	require.NoError(t, err)
}
