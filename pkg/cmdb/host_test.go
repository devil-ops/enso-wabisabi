package cmdb

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestHostList(t *testing.T) {
	got, err := c.Host.List(context.Background(), HostListOpts{})
	require.NoError(t, err)
	require.NotNil(t, got)
	require.NotEmpty(t, got)
}

func TestFilteredHostList(t *testing.T) {
	got, err := c.Host.List(context.Background(), HostListOpts{
		OnlySupportGroup: "some-group-that-never-exists",
	})
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Empty(t, got)
}

func TestHostFilterOperational(t *testing.T) {
	tests := map[string]struct {
		host Host
		opts HostListOpts
		want bool
	}{
		"match": {
			host: Host{OperationalStatus: "Operational"},
			opts: HostListOpts{OnlyOperational: true},
			want: true,
		},
		"no-match": {
			host: Host{OperationalStatus: "Non-Operational"},
			opts: HostListOpts{OnlyOperational: true},
			want: false,
		},
	}
	for desc, tt := range tests {
		got := HostFilterOnlyOperational(tt.host, &tt.opts)
		require.Equal(t, tt.want, got, desc)
	}
}

func TestHostFilterSupportGroup(t *testing.T) {
	tests := map[string]struct {
		host Host
		opts HostListOpts
		want bool
	}{
		"empty-match": {
			host: Host{SupportGroup: "foo"},
			opts: HostListOpts{OnlySupportGroup: ""},
			want: true,
		},
		"match": {
			host: Host{SupportGroup: "foo"},
			opts: HostListOpts{OnlySupportGroup: "foo"},
			want: true,
		},
		"no-match": {
			host: Host{SupportGroup: "foo"},
			opts: HostListOpts{OnlySupportGroup: "bar"},
			want: false,
		},
	}
	for desc, tt := range tests {
		got := HostFilterSupportGroup(tt.host, &tt.opts)
		require.Equal(t, tt.want, got, desc)
	}
}

func TestApplyAllHostFilters(t *testing.T) {
	got := ApplyAllHostFilters(
		[]Host{
			{Name: "foo"},
			{Name: "bar"},
		},
		&HostListOpts{},
	)
	require.Equal(t, 2, len(got))
}

func TestValidFQDNFilter(t *testing.T) {
	require.False(t, HostFilterValidName(Host{Name: "foo bar"}, &HostListOpts{OnlyValidNames: true}))
	require.False(t, HostFilterValidName(Host{Name: "foo.bar.edu 11-17 02:17 3"}, &HostListOpts{OnlyValidNames: true}))
	require.True(t, HostFilterValidName(Host{Name: "foo.bar"}, &HostListOpts{OnlyValidNames: true}))
}

func TestApplyAllHostPTRFilters(t *testing.T) {
	hosts := []*Host{
		{Name: "foo"},
		{Name: "bar"},
	}
	got := ApplyAllHostPTRFilters(hosts, &HostListOpts{})
	require.Equal(t, hosts, got)
}

func TestAnsibleInventoryWithCMDBHosts(t *testing.T) {
	hosts := Hosts{
		{
			FQDN:    "foo.bar",
			UsedFor: "prod",
		},
		{
			FQDN:    "foo-dev.bar",
			UsedFor: "dev",
		},
		{
			FQDN: "missing-env.bar",
		},
		{
			FQDN:    "empty-env.bar",
			UsedFor: "",
		},
	}

	i := hosts.AnsibleInventory()
	require.NotNil(t, i)
	require.IsType(t, AnsibleInventory{}, i)
	require.Equal(
		t,
		AnsibleInventory{"dev": []string{"foo-dev.bar"}, "prod": []string{"foo.bar"}, "unknown": []string{"missing-env.bar", "empty-env.bar"}},
		i,
	)

	var b strings.Builder
	i.Fprint(&b)
	require.Equal(
		t,
		"[dev]\nfoo-dev.bar\n\n[prod]\nfoo.bar\n\n[unknown]\nempty-env.bar\nmissing-env.bar\n\n",
		b.String(),
	)
}
