/*
Package cmdb handles the cmdb api calls. This should eventually be moved to
native enso, however there are still a few fields that only exist here
*/
package cmdb

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/hashicorp/go-retryablehttp"
	"github.com/spf13/viper"
	"moul.io/http2curl"
)

const (
	baseURL = "https://cmdb.oit.duke.edu"
)

// ClientConfig is the configuration for a client
type ClientConfig struct {
	Token string
}

// Response is the standard endpiont response
type Response struct {
	*http.Response
}

// Client interacts with the API
type Client struct {
	client    *http.Client
	UserAgent string
	// Config    ClientConfig
	Host    HostService
	BaseURL string
	token   string
}

// New returns a new CMDB client using functional options
func New(opts ...func(*Client)) *Client {
	c := &Client{
		UserAgent: "enso-wabisabi",
		BaseURL:   baseURL,
	}
	for _, opt := range opts {
		opt(c)
	}
	if c.client == nil {
		rc, err := newRetryable()
		if err != nil {
			panic(err)
		}
		c.client = rc
	}
	c.Host = &HostServiceOp{client: c}
	return c
}

// WithToken sets the token on a cmdb client
func WithToken(s string) func(*Client) {
	return func(c *Client) {
		c.token = s
	}
}

type retryLogger struct{}

// Error fulfills the LeveledLogger interface for the retry client
func (l retryLogger) Error(msg string, opts ...interface{}) {
	slog.Error(msg, opts...)
}

// Info fulfills the LeveledLogger interface for the retry client
func (l retryLogger) Info(msg string, opts ...interface{}) {
	slog.Info(msg, opts...)
}

// Debug fulfills the LeveledLogger interface for the retry client
func (l retryLogger) Debug(msg string, opts ...interface{}) {
	slog.Debug(msg, opts...)
}

// Warn fulfills the LeveledLogger interface for the retry client
func (l retryLogger) Warn(msg string, opts ...interface{}) {
	slog.Warn(msg, opts...)
}

func newRetryable() (*http.Client, error) {
	retryClient := retryablehttp.NewClient()
	retryClient.RetryMax = 10
	retryClient.HTTPClient.Timeout = 300 * time.Second
	retryClient.Logger = retryLogger{}

	transport := &http.Transport{
		TLSHandshakeTimeout: 300 * time.Second,
	}
	proxy := os.Getenv("HTTPS_PROXY")
	if proxy != "" {
		proxyU, err := url.Parse(proxy)
		if err != nil {
			return nil, err
		}
		transport.Proxy = http.ProxyURL(proxyU)
	}
	retryClient.HTTPClient.Transport = transport
	return retryClient.StandardClient(), nil
}

// NewClient returns a Client object with some sensible defaults
// Deprecated: use New() instead
func NewClient(config ClientConfig, httpClient *http.Client) *Client {
	if httpClient == nil {
		retryClient := retryablehttp.NewClient()
		retryClient.RetryMax = 10
		retryClient.HTTPClient.Timeout = 300 * time.Second

		transport := &http.Transport{
			TLSHandshakeTimeout: 300 * time.Second,
		}
		proxy := os.Getenv("HTTPS_PROXY")
		if proxy != "" {
			proxyU, err := url.Parse(proxy)
			if err != nil {
				panic(err)
			}
			transport.Proxy = http.ProxyURL(proxyU)
		}
		retryClient.HTTPClient.Transport = transport
		httpClient = retryClient.StandardClient()
	}

	c := &Client{
		// Config:    config,
		token:     config.Token,
		client:    httpClient,
		UserAgent: "enso-wabisabi",
		BaseURL:   baseURL,
	}

	c.Host = &HostServiceOp{client: c}
	return c
}

// NewClientWithEnv returns using credentials from the environment
func NewClientWithEnv() (*Client, error) {
	token := os.Getenv("CMDB_TOKEN")
	if token == "" {
		return nil, errors.New("must set CMDB_TOKEN when using NewClientWithEnv()")
	}
	return New(WithToken(token)), nil
}

// NewClientWithViper is a new Client object using credentials from a viper.Viper
func NewClientWithViper(v *viper.Viper) (*Client, error) {
	token := v.GetString("cmdb-token")
	if token == "" {
		return nil, errors.New("must set cmdb-token when using NewClientWithViper()")
	}
	return New(WithToken(token)), nil
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*Response, error) {
	req.Header.Add("Authorization", "Token "+c.token)

	command, _ := http2curl.GetCurlCommand(req)
	slog.Debug("curl command", "command", fmt.Sprint(command))

	res, err := c.client.Do(req)
	req.Close = true
	if err != nil {
		return nil, err
	}

	defer dclose(res.Body)

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		slog.Debug("got error", "error", errRes, "status", res.Status)
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(res.Status)
		}
	}

	b, _ := io.ReadAll(res.Body)
	if string(b) != "" {
		if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
			slog.Warn("invalid json returned", "body", string(b))
			return nil, err
		}
	} else {
		// When there is no body
		return nil, nil
	}
	r := &Response{Response: res}

	return r, nil
}

// ErrorResponse holds the data needed to show represent error
type ErrorResponse struct {
	Message string `json:"errors"`
}

func dclose(c io.Closer) {
	err := c.Close()
	if err != nil {
		slog.Warn("error closing item", "error", err)
	}
}
