package cmdb

import (
	"context"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"sort"
	"strings"

	"golang.org/x/net/idna"
)

// HostService holds all the methods for the Host endpoint in the cmdb
type HostService interface {
	List(context.Context, HostListOpts) (Hosts, error)
}

// HostServiceOp is the operator for the HostService
type HostServiceOp struct {
	client *Client
}

// HostListOpts is the options for listing hosts
type HostListOpts struct {
	OnlySupportGroup string `json:"only_support_group"`
	OnlyOperational  bool   `json:"only_operational"`
	OnlyValidNames   bool   `json:"only_valid_names"`
	// Deprecated in favor of OnlyOperational
	// OnlyOperationalStatus string `json:"only_operational_status"`
}

// Hosts represents multiple host objects
type Hosts []*Host

// Host represents a single Host object
type Host struct {
	ADDomain          interface{} `json:"ad_domain,omitempty"`
	AdminType         interface{} `json:"admin_type,omitempty"`
	Assettag          interface{} `json:"assettag,omitempty"`
	ClockworksURL     interface{} `json:"clockworks_url,omitempty"`
	ConfigManager     interface{} `json:"config_manager,omitempty"`
	CPU               interface{} `json:"cpu,omitempty"`
	CPUCores          interface{} `json:"cpu_cores,omitempty"`
	CPUModel          interface{} `json:"cpu_model,omitempty"`
	CreatedAt         interface{} `json:"created_at,omitempty"`
	CreatedBy         interface{} `json:"created_by,omitempty"`
	Criticality       interface{} `json:"criticality,omitempty"`
	CustomerGroup     interface{} `json:"customer_group,omitempty"`
	DBSupportGroup    interface{} `json:"db_support_group,omitempty"`
	Description       interface{} `json:"description,omitempty"`
	DiskGB            interface{} `json:"disk_gb,omitempty"`
	DisplayURL        string      `json:"display_url,omitempty"`
	ESXHost           interface{} `json:"esx_host,omitempty"`
	FQDN              string      `json:"fqdn,omitempty"`
	HostingLevel      interface{} `json:"hosting_level,omitempty"`
	InstallStatus     string      `json:"install_status,omitempty"`
	IsChassis         float64     `json:"ischassis,omitempty"`
	IsVirtual         float64     `json:"isvirtual,omitempty"`
	LegacyGroup       interface{} `json:"legacy_group,omitempty"`
	Location          interface{} `json:"location,omitempty"`
	ManagedBy         interface{} `json:"managed_by,omitempty"`
	Manufacturer      interface{} `json:"manufacturer,omitempty"`
	MemMb             interface{} `json:"mem_mb,omitempty"`
	Model             interface{} `json:"model,omitempty"`
	Name              string      `json:"name,omitempty"`
	Nwports           interface{} `json:"nwports,omitempty"`
	OperationalStatus string      `json:"operational_status,omitempty"`
	Org               string      `json:"org,omitempty"`
	OS                interface{} `json:"os,omitempty"`
	OSFamily          string      `json:"os_family,omitempty"`
	OSPatchLevel      interface{} `json:"os_patch_lvl,omitempty"`
	OSVersion         interface{} `json:"osversion,omitempty"`
	OwnedBy           interface{} `json:"owned_by,omitempty"`
	PatchWindow       interface{} `json:"patch_window,omitempty"`
	RackLocation      interface{} `json:"rack_location,omitempty"`
	SelfAdmin         interface{} `json:"self_admin,omitempty"`
	SerialNumber      interface{} `json:"serialnumber,omitempty"`
	SNSysID           interface{} `json:"sn_sys_id,omitempty"`
	SupportGroup      string      `json:"support_group,omitempty"`
	SupportURL        interface{} `json:"support_url,omitempty"`
	UpdatedAt         string      `json:"updated_at,omitempty"`
	UpdatedBy         interface{} `json:"updated_by,omitempty"`
	URL               string      `json:"url,omitempty"`
	UsedFor           interface{} `json:"used_for,omitempty"`
	UUID              string      `json:"uuid,omitempty"`
	VCenter           interface{} `json:"vcenter,omitempty"`
	VMId              interface{} `json:"vmid,omitempty"`
}

// List list hosts
func (svc *HostServiceOp) List(_ context.Context, opts HostListOpts) (Hosts, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, "/api/v1/servers.json"), nil)
	if err != nil {
		return nil, err
	}
	// Get all hosts
	var all []*Host
	_, err = svc.client.sendRequest(req, &all)
	if err != nil {
		return nil, err
	}

	return ApplyAllHostPTRFilters(all, &opts), nil
}

type (
	// HostFilter filters a single host
	HostFilter func(Host, *HostListOpts) bool
	// HostFilterBulk filters multiple hosts
	HostFilterBulk func([]Host, *HostListOpts) []Host
)

// ApplyHostFilters applies a set of filters to a set of hosts
func ApplyHostFilters(records []Host, opts *HostListOpts, filters ...HostFilter) []Host {
	if len(filters) == 0 {
		return records
	}
	filteredRecords := make([]Host, 0, len(records))

	for _, r := range records {
		keep := true

		for _, f := range filters {
			if !f(r, opts) {
				keep = false
				break
			}
		}

		if keep {
			filteredRecords = append(filteredRecords, r)
		}
	}

	return filteredRecords
}

// ApplyAllHostFilters applies all the available filters to a set of hosts
func ApplyAllHostFilters(records []Host, opts *HostListOpts) []Host {
	return ApplyHostFilters(
		records,
		opts,
		HostFilterSupportGroup,
		HostFilterOnlyOperational,
		HostFilterValidName,
	)
}

// ApplyAllHostPTRFilters applies all the available filters to a set of host pointers
func ApplyAllHostPTRFilters(records []*Host, opts *HostListOpts) []*Host {
	rslved := make([]Host, len(records))
	for idx, item := range records {
		rslved[idx] = *item
	}
	filtered := ApplyHostFilters(
		rslved,
		opts,
		HostFilterSupportGroup,
		HostFilterOnlyOperational,
		HostFilterValidName,
	)

	ret := make([]*Host, len(filtered))
	for nidx, nitem := range filtered {
		ret[nidx] = &nitem
	}
	return ret
}

// HostFilterSupportGroup filters a host based on a support group
func HostFilterSupportGroup(h Host, o *HostListOpts) bool {
	if o.OnlySupportGroup == "" {
		return true
	}
	return strings.EqualFold(h.SupportGroup, o.OnlySupportGroup)
}

// HostFilterValidName filters a host based on if the fqdn is actually valid.
// Sometimes we get back names with spaces or other stuff that cause errors when
// putting them in to things like Ansible that expect a valid fqdn for the name
func HostFilterValidName(h Host, o *HostListOpts) bool {
	if !o.OnlyValidNames {
		return true
	}
	if _, err := idna.Lookup.ToASCII(h.Name); err != nil {
		slog.Warn("skipping invalid name", "name", h.Name)
		return false
	}
	return true
}

/*
Deprecating this for the more simple HostFilterOnlyOperational
func HostFilterOnlyOperationalStatus(h Host, o *HostListOpts) bool {
	if o.OnlyOperationalStatus == "" {
		return true
	}
	return strings.EqualFold(h.OperationalStatus, o.OnlyOperationalStatus)
}
*/

// HostFilterOnlyOperational returns hosts that are operational
func HostFilterOnlyOperational(h Host, o *HostListOpts) bool {
	if !o.OnlyOperational {
		return true
	}
	return strings.EqualFold(h.OperationalStatus, "Operational")
}

// AnsibleInventory represents an Ansible Inventory go object
type AnsibleInventory map[string][]string

// Fprint prints the inventory out to an io.Writer
func (i AnsibleInventory) Fprint(out io.Writer) {
	// Sort the environments for reproducible output
	sortedEnvs := []string{}
	for k := range i {
		sortedEnvs = append(sortedEnvs, k)
	}
	sort.Strings(sortedEnvs)

	// Ok, got it all collected, let's print it
	for _, env := range sortedEnvs {
		if _, err := fmt.Fprint(out, "[", env, "]\n"); err != nil {
			panic(err)
		}
		// Sort hosts to give consistent output
		sort.Strings(i[env])
		for _, host := range i[env] {
			if _, err := fmt.Fprintln(out, host); err != nil {
				panic(err)
			}
		}
		// Little spacing in between sections
		if _, err := fmt.Fprint(out, "\n"); err != nil {
			panic(err)
		}
	}
}

// AnsibleInventory returns an AnsibleInventory based on existing CMDB hosts
func (chs Hosts) AnsibleInventory() AnsibleInventory {
	inventory := AnsibleInventory{}

	for _, h := range chs {
		// Init used_for if it doesn't exist
		environment, ok := h.UsedFor.(string)
		if !ok {
			// Sometimes this is not set correctly, just flip it to unknown
			slog.Debug("could not convert used_for to a string, setting it to 'unknown'", "used_for", h.UsedFor)
			environment = "unknown"
		}
		if environment == "" {
			// Sometimes it's an empty string, also set that to unknown
			environment = "unknown"
		}
		// Always make environment lowercase
		environment = strings.ToLower(environment)

		if _, ok := inventory[environment]; !ok {
			inventory[environment] = []string{}
		}

		inventory[environment] = append(inventory[environment], h.FQDN)
	}
	return inventory
}
