/*
Package main is the main executable
*/
package main

import "gitlab.oit.duke.edu/devil-ops/enso-wabisabi/cmd/enso-wabisabi/cmd"

func main() {
	cmd.Execute()
}
