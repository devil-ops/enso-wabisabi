package cmd

import (
	"context"
	"fmt"
	"log/slog"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/enso-wabisabi/pkg/alertmanager"
	"gopkg.in/yaml.v3"
)

// generateAlertManagerRulesCmd represents the generateAlertManagerRules command
var generateAlertManagerRulesCmd = &cobra.Command{
	Use:   "alert-manager-rules",
	Short: "Generate YAML for Prometheus Alert Manager rules",
	Args:  cobra.NoArgs,
	RunE: func(_ *cobra.Command, _ []string) error {
		templates, _, err := c.Monitor.Templates(context.TODO())
		if err != nil {
			return err
		}
		thresholds := []string{"warn", "crit"}
		amc := alertmanager.Config{}
		for _, template := range templates {
			if len(template.Metrics) == 0 {
				slog.Debug("skipping entry with no metrics", "template", template.Name)
				continue
			}

			var groups []alertmanager.Group
			for _, metric := range template.Metrics {
				var group alertmanager.Group
				group.Name = metric.Name

				for _, threshold := range thresholds {
					amMetricName := fmt.Sprintf("%v_%v", metric.Name, threshold)
					d, aerr := alertmanager.Defaults(amMetricName)
					if aerr != nil {
						slog.Debug("no defaults", "metric", amMetricName, "error", aerr)
						continue
					}
					d.Annotations.Description = metric.Description
					switch threshold {
					case "crit":
						d.Expr = strings.ReplaceAll(d.Expr, "THRESH", fmt.Sprint(*metric.ThresholdCrit))
					case "warn":
						d.Expr = strings.ReplaceAll(d.Expr, "THRESH", fmt.Sprint(*metric.ThresholdWarn))
					}
					group.Rules = append(group.Rules, *d)

				}
				if len(group.Rules) > 0 {
					groups = append(groups, group)
				}
			}
			amc.Groups = append(amc.Groups, groups...)
		}
		yammy, err := yaml.Marshal(&amc)
		if err != nil {
			return err
		}
		fmt.Println("---")
		fmt.Println(string(yammy))
		return nil
	},
}

func init() {
	generateCmd.AddCommand(generateAlertManagerRulesCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateAlertManagerRulesCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateAlertManagerRulesCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
