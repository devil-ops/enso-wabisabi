package cmd

import (
	"fmt"
	"log/slog"
	"sort"
	"time"

	"gitlab.oit.duke.edu/devil-ops/enso-wabisabi/pkg/enso"

	"github.com/spf13/cobra"
)

// getHostCmd represents the getHost command
var getHostCmd = &cobra.Command{
	Use:     "host",
	Aliases: []string{"hosts"},
	Short:   "Get host listing",
	RunE: func(cmd *cobra.Command, args []string) error {
		opts, err := enso.HostListEntryFilterOptsWithCobra(cmd, args)
		if err != nil {
			return err
		}

		slog.Info("Retrieving hosts...")
		hosts, _, err := c.Host.List(nil)
		if err != nil {
			return err
		}

		hosts = enso.ApplyHostListEntryFiltersWithOpts(hosts, opts)

		sort.Slice(hosts, func(i, j int) bool {
			return hosts[i].Name < hosts[j].Name
		})

		if mustGetCmd[bool](*cmd, "full-details") {
			outter.MustPrint(hosts)
		} else {
			for _, item := range hosts {
				fmt.Println(item.Name)
			}
		}
		slog.Info("summary", "count", len(hosts), "duration", fmt.Sprint(time.Since(started)))
		return nil
	},
}

func init() {
	getCmd.AddCommand(getHostCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	getHostCmd.PersistentFlags().String("name", "", "Only show hosts that include this string in the name")
	getHostCmd.PersistentFlags().BoolP("full-details", "f", false, "Show full information on the hosts, instead of just the name")
	getHostCmd.PersistentFlags().Bool("valid-names", false, "Only show entries with valid names (Such as a real FQDN)")

	// Filter based on IP
	getHostCmd.PersistentFlags().Bool("only-public-ips", false, "Only return entries with a public IP")
	getHostCmd.PersistentFlags().Bool("only-private-ips", false, "Only return entries with a private IP")
	getHostCmd.MarkFlagsMutuallyExclusive("only-public-ips", "only-private-ips")

	getHostCmd.PersistentFlags().Bool("only-monitored", false, "Only return entries with some sort of monitoring set")
	// getHostCmd.PersistentFlags().Bool("only-unmonitored", false, "Only return entries with no monitoring set")
	// getHostCmd.MarkFlagsMutuallyExclusive("only-monitored", "only-unmonitored")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getHostCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
