package cmd

import (
	"context"

	"github.com/spf13/cobra"
)

// generatePrometheusBlackboxTargetsCmd represents the generatePrometheusBlackboxTargets command
var generatePrometheusBlackboxTargetsCmd = &cobra.Command{
	Use:   "prometheus-blackbox-targets",
	Short: "Generates blackbox targets based on enso pingable device info",
	Args:  cobra.NoArgs,
	RunE: func(cmd *cobra.Command, _ []string) error {
		devices, _, err := c.Host.Pingable(context.Background())
		if err != nil {
			return err
		}
		return marshalToFile(devices.Targets(), mustGetCmd[string](*cmd, "output-file"))
	},
}

func init() {
	generateCmd.AddCommand(generatePrometheusBlackboxTargetsCmd)
	generatePrometheusBlackboxTargetsCmd.Flags().String("output-file", "", "Output to the given filepath instead of stdout")
}
