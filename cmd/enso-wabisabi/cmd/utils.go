package cmd

import (
	"cmp"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net"
	"net/netip"
	"os"
	"path"
	"reflect"
	"slices"
	"sort"
	"strings"
	"time"

	"github.com/helmfile/vals"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
	"gopkg.in/yaml.v2"
)

type promHostTargetConfig struct {
	Labels  promHostLabels `json:"labels,omitempty"`
	Targets []string       `json:"targets,omitempty"`
}

type promHostLabels struct {
	Instance string `json:"instance,omitempty"`
	Job      string `json:"job,omitempty"`
	VRF      string `json:"vrf,omitempty"`
}

func float64Or(s *float64) string {
	if s == nil {
		return unknown
	}
	return fmt.Sprint(*s)
}

const unknown = "Unknown"

func newNetworkTargetWithDevice(device cartographer.Device) promNetworkTargetConfig {
	t := promNetworkTargetConfig{
		Targets: []string{*device.ManagementIp},
		Labels: promNetworkLabels{
			BuildingID:      float64Or(device.BuildingID),
			Function:        cmp.Or(device.Function, unknown),
			LanID:           float64Or(device.LanID),
			Model:           cmp.Or(device.Model, unknown),
			Name:            cmp.Or(device.Name, unknown),
			SerialNumber:    cmp.Or(device.SerialNumber, unknown),
			SoftwareVersion: cmp.Or(device.SoftwareVersion, unknown),
		},
	}
	if (device.BuildingLocationName == nil) || (*device.BuildingLocationName == "") {
		t.Labels.BuildingLocationName = "Unknown"
	} else {
		t.Labels.BuildingLocationName = *device.BuildingLocationName
	}
	return t
}

// just like marshalToFile, but do a panic instead of returning an error
func mustMarshalToFile(i interface{}, path string) {
	if err := marshalToFile(i, path); err != nil {
		panic(err)
	}
}

// Given an interface and filepath, marshal json out to it. If given an empty
// string for the filepath, use stdout
func marshalToFile(i any, fpath string) error {
	var outF *os.File
	var err error
	if fpath == "" {
		outF = os.Stdout
	} else {
		outF, err = os.Create(path.Clean(fpath))
		if err != nil {
			return err
		}
	}
	return marshalTo(i, outF)
}

func marshalTo(i any, w io.Writer) error {
	b, err := json.MarshalIndent(i, "", "  ")
	if err != nil {
		return err
	}
	_, err = w.Write(b)
	return err
}

// Given a list of vrfs, return the first one alphabetically. If given an empty
// list, return an empty string
func vrfWithVRFs(vrfs []string) string {
	switch num := len(vrfs); {
	case num == 1:
		return vrfs[0]
	case num > 1:
		sort.Strings(vrfs)
		return vrfs[0]
	default:
		return ""
	}
}

func bindJobCobra(cmd *cobra.Command) error {
	cmd.Flags().Int("metrics-port", 9273, "Port to use when collecting metrics")
	cmd.Flags().String("job", "", "Name to use for job (Something like 'linux_collect' or 'windows_collect' in general")
	err := cmd.MarkFlagRequired("job")
	if err != nil {
		return err
	}
	cmd.Flags().String("output-file", "", "Output to the given filepath instead of stdout")
	return nil
}

type jobOpts struct {
	MetricsPort int
	Name        string
	OutputFile  string
}

func jobOptsWithCobra(cmd *cobra.Command) *jobOpts {
	return &jobOpts{
		Name:        mustGetCmd[string](*cmd, "job"),
		MetricsPort: mustGetCmd[int](*cmd, "metrics-port"),
		OutputFile:  mustGetCmd[string](*cmd, "output-file"),
	}
}

// mustGetCmd uses generics to get a given flag with the appropriate Type from a cobra.Command
func mustGetCmd[T []int | []string | float64 | int | string | bool | time.Duration](cmd cobra.Command, s string) T {
	switch any(new(T)).(type) {
	case *int:
		item, err := cmd.Flags().GetInt(s)
		panicIfErr(err)
		return any(item).(T)
	case *float64:
		item, err := cmd.Flags().GetFloat64(s)
		panicIfErr(err)
		return any(item).(T)
	case *string:
		item, err := cmd.Flags().GetString(s)
		panicIfErr(err)
		return any(item).(T)
	case *bool:
		item, err := cmd.Flags().GetBool(s)
		panicIfErr(err)
		return any(item).(T)
	case *[]int:
		item, err := cmd.Flags().GetIntSlice(s)
		panicIfErr(err)
		return any(item).(T)
	case *[]string:
		item, err := cmd.Flags().GetStringSlice(s)
		panicIfErr(err)
		return any(item).(T)
	case *time.Duration:
		item, err := cmd.Flags().GetDuration(s)
		panicIfErr(err)
		return any(item).(T)
	default:
		panic(fmt.Sprintf("unexpected use of mustGetCmd: %v", reflect.TypeOf(s)))
	}
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

func toPTR[V any](v V) *V {
	return &v
}

/*
func fromPTR[V any](v *V) V {
	return *v
}
*/

// LookupIP is a simple dns lookup
func lookupIP(fqdn string) (string, error) {
	if !strings.Contains(fqdn, ".") {
		return "", fmt.Errorf("lookup failed because %v must contain a '.'", fqdn)
	}
	ips, err := net.LookupIP(fqdn)
	if err != nil {
		return "", err
	}
	if len(ips) == 0 {
		return "", fmt.Errorf("no ips found for name: %v", fqdn)
	}
	if len(ips) > 1 {
		slog.Warn("fqdn lookup returned multiple IPs, using the first one", "fqdn", fqdn)
		return ips[0].String(), nil
	}
	return ips[0].String(), nil
}

func cartographerVRFMapping(username, token string) (map[string][]string, error) {
	cartC := cartographer.NewClient(cartographer.ClientConfig{
		APIUser: username,
		APIKey:  token,
	}, nil)
	cartC.BaseURL = "https://cartographer.oit.duke.edu"
	vrfs, _, err := cartC.VRF.List(nil)
	if err != nil {
		return nil, err
	}
	ret := map[string][]string{}
	for _, vrf := range vrfs {
		if _, ok := ret[vrf.Name]; !ok {
			ret[vrf.Name] = []string{}
		}
		vinfo, _, err := cartC.VRF.Get(nil, vrf.Name)
		if err != nil {
			return nil, err
		}
		for _, net := range vinfo.Subnets {
			ret[vrf.Name] = append(ret[vrf.Name], net.String())
		}
	}
	return ret, nil
}

// detectVRFs does the vrf detection
func detectVRFs(mapping map[string][]string, ip string) ([]string, error) {
	ignoreVRFs := []string{"default", "datacenter", "dc-lb"}
	inVrfs := []string{}
	a, err := netip.ParseAddr(ip)
	if err != nil {
		return nil, err
	}
	for vrfName, networks := range mapping {
		if slices.Contains(ignoreVRFs, vrfName) {
			continue
		}
		for _, network := range networks {
			n, err := netip.ParsePrefix(network)
			if err != nil {
				slog.Warn("error parsing network, skipping", "network", network)
				continue
			}
			if n.Contains(a) {
				if !slices.Contains(inVrfs, vrfName) {
					inVrfs = append(inVrfs, vrfName)
				}
			}
		}
	}
	return inVrfs, nil
}

// expandVRFPattern blows a pattern up
func expandVRFPattern(possibles []string, pattern string) []string {
	pparts := strings.Split(pattern, ",")
	targetVRFs := []string{}
	negateVRFs := []string{}

	for _, part := range pparts {
		switch {
		case strings.HasPrefix(part, "!"):
			negateVRFs = append(negateVRFs, part[1:])
		case part == "all":
			targetVRFs = append(targetVRFs, possibles...)
		case slices.Contains(possibles, part):
			targetVRFs = append(targetVRFs, part)
		default:
			slog.Warn("vrf does not exist", "vrf", part)
		}
	}

	combined := []string{}
	for _, vrf := range targetVRFs {
		if !slices.Contains(negateVRFs, vrf) {
			combined = append(combined, vrf)
		}
	}
	return combined
}

// enhanceViper applies the vals stuff to a viper.Viper
func enhanceViper(_ *viper.Viper, yf string) error {
	secrets, err := os.ReadFile(yf) // nolint:gosec
	if err != nil {
		return err
	}
	runtime, err := vals.New(vals.Options{
		CacheSize: 256,
	})
	if err != nil {
		return err
	}
	secretsM := map[string]interface{}{}
	if err := yaml.Unmarshal(secrets, &secretsM); err != nil {
		return err
	}
	secretsRendered, err := runtime.Eval(secretsM)
	if err != nil {
		return err
	}

	for k, v := range secretsRendered {
		viper.Set(k, v)
	}
	return nil
}

// This function is used to get the config file path from the environment variable.
