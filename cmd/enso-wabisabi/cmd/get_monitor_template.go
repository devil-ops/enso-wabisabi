package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// getMonitorTemplateCmd represents the getMonitorTemplate command
var getMonitorTemplateCmd = &cobra.Command{
	Use:   "monitor-template",
	Short: "Get monitor templates",
	Args:  cobra.NoArgs,
	RunE: func(cmd *cobra.Command, _ []string) error {
		gout.SetFormatter(gout.BuiltInFormatters[mustGetCmd[string](*cmd, "output-format")])
		ts, _, err := c.Monitor.Templates(context.TODO())
		if err != nil {
			return err
		}
		return gout.Print(ts)
	},
}

func init() {
	getCmd.AddCommand(getMonitorTemplateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getMonitorTemplateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getMonitorTemplateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
