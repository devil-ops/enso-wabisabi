package cmd

import (
	"context"
	"fmt"
	"log/slog"
	"slices"
	"sync"
	"sync/atomic"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/enso-wabisabi/pkg/cmdb"
)

// generatePrometheusHostTargetsCmd represents the generatePrometheusHostTargets command
var generatePrometheusHostTargetsCmd = &cobra.Command{
	Use:   "prometheus-host-targets SUPPORT_GROUP VRF_PATTERN",
	Args:  cobra.ExactArgs(2),
	Short: "Generate prometheus file containing host targets using data from the CMDB",
	Long: `SUPPORT_GROUP will most likely be either Systems-UNIX-OIT or Systems-Windows-OIT

VRF_PATTERN is a comma separated list of VRFs. Only hosts in these VRFs will be
included. If you use the special name 'all', it will include all VRFs. You can
also negate VRFs by prepending a '!'. Example: 'all,!datacenter-1' would include
all VRFs expect hosts in the 'datacenter-1'. This is how we get the 'catchall'
stuff to work`,
	RunE: func(cmd *cobra.Command, args []string) error {
		jo := jobOptsWithCobra(cmd)

		// Pre-wait-group waits for some long api calls before continuing
		var wg sync.WaitGroup
		wg.Add(2)

		var mapping map[string][]string
		var targetVRFs []string
		go func() {
			defer wg.Done()
			var err error
			slog.Info("getting vrf to network mapping from Cartographer")
			mapping, err = cartographerVRFMapping(viper.GetString("cartographer-user"), viper.GetString("cartographer-token"))
			if err != nil {
				panic(err)
			}
			// Get keys from mapping for vrf names
			keys := make([]string, len(mapping))
			i := 0
			for k := range mapping {
				keys[i] = k
				i++
			}
			// Expand out the pattern to individual vrfs
			targetVRFs = expandVRFPattern(keys, args[1])
		}()

		var allHosts []*cmdb.Host
		go func() {
			defer wg.Done()
			cmdbC, err := cmdb.NewClientWithViper(viper.GetViper())
			if err != nil {
				panic(err)
			}

			slog.Info("looking up hosts from the cmdb", "support-group", args[0])
			allHosts, err = cmdbC.Host.List(context.Background(), cmdb.HostListOpts{
				OnlySupportGroup: args[0],
				OnlyOperational:  true,
			})
			if err != nil {
				panic(err)
			}
		}()
		wg.Wait()
		slog.Debug("Got pre-work pieces done, continuing")

		// Not sure if this is overkill, but using a sync.Map, we don't
		// need to complicate things with channels, or slow things down
		// with mutexes. After trying both of those previous approaches,
		// this seems to be the fastest with the least complexity - DS
		var tinfosMap sync.Map
		var goodHostCount int64

		wg.Add(len(allHosts))
		for idx, host := range allHosts {
			go func(host *cmdb.Host, idx int) {
				defer wg.Done()

				ip, err := lookupIP(host.FQDN)
				if err != nil {
					slog.Warn("skipping fqn because the ip lookup failed", "error", err)
					return
				}
				slogger := slog.With("fqdn", host.FQDN, "ip", ip)
				vrfs, err := detectVRFs(mapping, ip)
				if err != nil {
					panic(err)
				}

				// Get the primary vrf from a list of vrfs. In
				// reality, if it gets more than 1, it's just
				// gonna return the first alphabetically
				targetVRF := vrfWithVRFs(vrfs)
				if targetVRF == "" {
					slogger.Warn("no vrfs found")
					return
				}

				// Do stuff here
				if slices.Contains(targetVRFs, targetVRF) {
					// if utils.ContainsString(targetVRFs, targetVRF) {
					slogger.Debug("got host in one of the target vrfs", "vrf", targetVRF)
					tinfosMap.Store(idx, promHostTargetConfig{
						Targets: []string{fmt.Sprintf("%v:%v", ip, jo.MetricsPort)},
						Labels: promHostLabels{
							Instance: host.FQDN,
							Job:      jo.Name,
							VRF:      targetVRF,
						},
					})
					// tinfoCount.Add(1)
					atomic.AddInt64(&goodHostCount, 1)
				}
			}(host, idx)
		}
		wg.Wait()

		// Yoink out the values in to an array so we can easily marshal it
		tinfos := make([]promHostTargetConfig, goodHostCount)
		var idx int
		tinfosMap.Range(func(_ any, v any) bool {
			tinfos[idx] = v.(promHostTargetConfig)
			idx++
			return true
		})

		slog.Info("found devices", "count", len(tinfos), "host-count", goodHostCount)
		return marshalToFile(tinfos, jo.OutputFile)
	},
}

func init() {
	generateCmd.AddCommand(generatePrometheusHostTargetsCmd)
	if err := bindJobCobra(generatePrometheusHostTargetsCmd); err != nil {
		panic(err)
	}
}
