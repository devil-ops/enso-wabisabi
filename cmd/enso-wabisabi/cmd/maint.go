package cmd

import (
	"errors"

	"github.com/spf13/cobra"
)

var maintCmd = &cobra.Command{
	Use:     "maintenance",
	Short:   "Start or stop maintenance for a host",
	Aliases: []string{"maint", "m"},
	RunE: func(cmd *cobra.Command, _ []string) error {
		return errors.New(cmd.UsageString())
	},
}

func init() {
	rootCmd.AddCommand(maintCmd)
}
