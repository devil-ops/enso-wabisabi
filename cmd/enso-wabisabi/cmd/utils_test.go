package cmd

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func TestNewNetworkTarget(t *testing.T) {
	require.Equal(t,
		promNetworkTargetConfig{
			Labels: promNetworkLabels{
				BuildingID:           "4.5",
				BuildingLocationName: "some street",
				Function:             "housing",
				LanID:                "1.2",
				Model:                "some-model",
				Name:                 "some-name",
				SerialNumber:         "1234-5678",
				SoftwareVersion:      "0.1.2",
			},
			Targets: []string{"1.2.3.4"},
		},
		newNetworkTargetWithDevice(cartographer.Device{
			ManagementIp:         toPTR("1.2.3.4"),
			BuildingID:           toPTR(4.5),
			BuildingLocationName: toPTR("some street"),
			Function:             "housing",
			LanID:                toPTR(1.2),
			Model:                "some-model",
			Name:                 "some-name",
			SerialNumber:         "1234-5678",
			SoftwareVersion:      "0.1.2",
		}), "testing all fields present")
	require.Equal(t,
		promNetworkTargetConfig{
			Labels: promNetworkLabels{
				BuildingID:           "Unknown",
				BuildingLocationName: "Unknown",
				Function:             "Unknown",
				LanID:                "1.2",
				Model:                "some-model",
				Name:                 "some-name",
				SerialNumber:         "1234-5678",
				SoftwareVersion:      "0.1.2",
			},
			Targets: []string{"1.2.3.4"},
		},
		newNetworkTargetWithDevice(cartographer.Device{
			ManagementIp:    toPTR("1.2.3.4"),
			LanID:           toPTR(1.2),
			Model:           "some-model",
			Name:            "some-name",
			SerialNumber:    "1234-5678",
			SoftwareVersion: "0.1.2",
		}), "testing missing some fields")
}

func TestDetectVRFs(t *testing.T) {
	got, err := detectVRFs(map[string][]string{
		"default":       {"10.0.0.1/24"},
		"foo":           {"10.0.0.1/24"},
		"not-a-network": {"hi"},
	}, "10.0.0.2")
	require.NoError(t, err)
	require.Equal(t, []string{"foo"}, got)

	// Test bad ip
	got, err = detectVRFs(map[string][]string{
		"default": {"10.0.0.1/24"},
	}, "yoyoyo")
	require.Error(t, err)
	require.EqualError(t, err, "ParseAddr(\"yoyoyo\"): unable to parse IP")
	require.Nil(t, got)
}

func TestExpandVRFPattern(t *testing.T) {
	everything := []string{"foo", "bar", "baz", "qux"}

	tests := map[string]struct {
		pattern string
		want    []string
	}{
		"simple": {
			pattern: "bar",
			want:    []string{"bar"},
		},
		"multiple": {
			pattern: "foo,bar",
			want:    []string{"foo", "bar"},
		},
		"negate-all": {
			pattern: "all,!bar",
			want:    []string{"foo", "baz", "qux"},
		},
		"unknown-item": {
			pattern: "foo,bar,bingo",
			want:    []string{"foo", "bar"},
		},
	}
	for desc, tt := range tests {
		require.Equal(t, tt.want, expandVRFPattern(everything, tt.pattern), desc)
	}
}
