package cmd

import (
	"context"
	"fmt"
	"log/slog"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/vmware/govmomi/property"
	"github.com/vmware/govmomi/view"
	"github.com/vmware/govmomi/vim25/mo"

	// "github.com/vmware/govmomi/find"

	"gitlab.oit.duke.edu/devil-ops/enso-wabisabi/pkg/horizon"
)

// generatePrometheusHorizonTargetsCmd represents the generatePrometheusHorizonTargets command
var generatePrometheusHorizonTargetsCmd = &cobra.Command{
	Use:        "prometheus-horizon-targets",
	Short:      "Generate prometheus job file with VMware Horizon RDS targets",
	Hidden:     true,
	Deprecated: "this command will eventually be removed since we aren't really using horizon at this point",
	Args:       cobra.NoArgs,
	RunE: func(cmd *cobra.Command, _ []string) error {
		hc, err := horizon.NewWithViper(viper.GetViper())
		if err != nil {
			return err
		}

		jo := jobOptsWithCobra(cmd)

		items, err := hc.Inventory.RDSServers()
		if err != nil {
			return err
		}

		vs, err := horizon.NewVMWareClientWithViper(viper.GetViper())
		if err != nil {
			return err
		}

		tinfos := make([]promHostTargetConfig, len(items))

		// Generate an API view in here for looking up the guest IP
		m := view.NewManager(vs.Client)
		v, err := m.CreateContainerView(context.TODO(), vs.Client.ServiceContent.RootFolder, []string{"VirtualMachine"}, true)
		if err != nil {
			return err
		}

		for idx, item := range items {

			var vms []mo.VirtualMachine
			filter := property.Filter{"guest.hostName": item.DNSName} // The filter is guest.hostName (<- that capital 'N' is very important)
			if err := v.RetrieveWithFilter(context.TODO(), []string{"VirtualMachine"}, nil, &vms, filter); err != nil {
				slog.Warn("error using filter", "name", item.DNSName, "error", err)
				continue
			}

			// Do some sanity checks on the vms that got filtered
			if len(vms) == 0 {
				slog.Warn("could not find any VMs matching that name", "name", item.DNSName)
			} else if len(vms) > 1 {
				slog.Warn("found more than 1 VM matching this name, using the first one", "name", item.DNSName)
			}

			tinfos[idx] = promHostTargetConfig{
				Targets: []string{fmt.Sprintf("%v:%v", vms[0].Guest.IpAddress, jo.MetricsPort)},
				Labels: promHostLabels{
					Instance: item.DNSName,
					Job:      jo.Name,
				},
			}
		}

		mustMarshalToFile(tinfos, jo.OutputFile)
		return nil
	},
}

func init() {
	generateCmd.AddCommand(generatePrometheusHorizonTargetsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generatePrometheusHorizonTargetsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generatePrometheusHorizonTargetsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	err := bindJobCobra(generatePrometheusHorizonTargetsCmd)
	cobra.CheckErr(err)
}
