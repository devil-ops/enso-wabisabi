package cmd

import (
	"context"
	"fmt"
	"log/slog"
	"time"

	"github.com/spf13/cobra"
)

var maintStartCmd = &cobra.Command{
	Use:   "start HOST|IP DURATION",
	Short: "Put a host in maintenance mode",
	Long: `DURATION must be a valid duration in go, documented here: https://pkg.go.dev/time#Duration

Examples:
enso-wabisabi maint start host-01.example.edu 30m`,
	Args: cobra.ExactArgs(2),
	RunE: func(_ *cobra.Command, args []string) error {
		duration, err := time.ParseDuration(args[1])
		if err != nil {
			return err
		}
		slog.Info("putting host in maint mode", "host", args[0], "duration", fmt.Sprint(duration))
		res, err := c.Maintenance.Start(context.Background(), args[0], duration)
		if err != nil {
			return err
		}
		slog.Info("response", "response", res.Response)
		return nil
	},
}

func init() {
	maintCmd.AddCommand(maintStartCmd)
}
