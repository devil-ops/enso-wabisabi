package cmd

import (
	"context"
	"io"
	"log/slog"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/enso-wabisabi/pkg/cmdb"
)

// generateAnsibleInventoryCmd represents the generateAnsibleInventory command
var generateAnsibleInventoryCmd = &cobra.Command{
	Use:   "ansible-inventory SUPPORT_GROUP",
	Short: "Generate an Ansible Inventory from the CMDB",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		cmdbC, err := cmdb.NewClientWithViper(viper.GetViper())
		if err != nil {
			return err
		}

		outA, err := cmd.Flags().GetString("output-file")
		if err != nil {
			return err
		}

		slog.Info("looking up hosts from the cmdb", "support-group", args[0])
		allHosts, err := cmdbC.Host.List(context.Background(), cmdb.HostListOpts{
			OnlySupportGroup: args[0],
			OnlyOperational:  true,
			OnlyValidNames:   true,
		})
		if err != nil {
			return err
		}

		inventory := allHosts.AnsibleInventory()

		// Don't open the writer until we are ready please
		var outF io.Writer
		if outA == "" {
			outF = os.Stdout
		} else {
			outF, err = os.Create(outA) // nolint:gosec
			if err != nil {
				return err
			}
		}

		inventory.Fprint(outF)
		return nil
	},
}

func init() {
	generateCmd.AddCommand(generateAnsibleInventoryCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	generateAnsibleInventoryCmd.PersistentFlags().String("output-file", "", "Write output to a file. Defaults to stdout")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateAnsibleInventoryCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
