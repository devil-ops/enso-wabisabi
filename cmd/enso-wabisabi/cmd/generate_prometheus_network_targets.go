package cmd

import (
	"context"
	"log/slog"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

// generatePrometheusNetworkTargetsCmd represents the generatePrometheusNetworkTargets command
var generatePrometheusNetworkTargetsCmd = &cobra.Command{
	Use:   "prometheus-network-targets",
	Short: "Generates networking targets based on Cartographer device info",
	Args:  cobra.NoArgs,
	RunE: func(cmd *cobra.Command, _ []string) error {
		outFile := mustGetCmd[string](*cmd, "output-file")
		cart, err := cartographer.NewClientWithViper(nil, viper.GetViper())
		if err != nil {
			return err
		}

		devices, _, err := cart.Device.List(toPTR(context.Background()))
		if err != nil {
			return err
		}

		promConfig := []promNetworkTargetConfig{}

		for _, device := range devices {

			dlog := slog.With("device", device.Name)
			// If we don't even have a management ip, skip completely
			if device.ManagementIp == nil {
				dlog.Warn("Device has no management IP, skipping")
				continue

			}
			promConfig = append(promConfig, newNetworkTargetWithDevice(device))
		}
		return marshalToFile(promConfig, outFile)
	},
}

func init() {
	generateCmd.AddCommand(generatePrometheusNetworkTargetsCmd)
	generatePrometheusNetworkTargetsCmd.Flags().String("output-file", "", "Output to the given filepath instead of stdout")
}

type promNetworkTargetConfig struct {
	Labels  promNetworkLabels `json:"labels,omitempty"`
	Targets []string          `json:"targets,omitempty"`
}

type promNetworkLabels struct {
	BuildingID           string `json:"building_id,omitempty"`
	BuildingLocationName string `json:"building_location_name,omitempty"`
	Function             string `json:"function,omitempty"`
	LanID                string `json:"lan_id,omitempty"`
	Model                string `json:"model,omitempty"`
	Name                 string `json:"name,omitempty"`
	SerialNumber         string `json:"serial_number,omitempty"`
	SoftwareVersion      string `json:"software_version,omitempty"`
}
