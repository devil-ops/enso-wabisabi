package cmd

import (
	"context"
	"log/slog"

	"github.com/spf13/cobra"
)

var maintEndCmd = &cobra.Command{
	Use:     "end HOST|IP",
	Short:   "Remove a host from maintenance mode",
	Args:    cobra.ExactArgs(1),
	Aliases: []string{"stop"},
	RunE: func(_ *cobra.Command, args []string) error {
		slog.Info("ending host maint mode", "host", args[0])
		res, err := c.Maintenance.End(context.Background(), args[0])
		if err != nil {
			return err
		}
		slog.Info("response", "response", res.Response)
		return nil
	},
}

func init() {
	maintCmd.AddCommand(maintEndCmd)
}
