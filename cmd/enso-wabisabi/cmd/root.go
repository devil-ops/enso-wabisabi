/*
Package cmd is the command line utility
*/
package cmd

import (
	"log/slog"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/charmbracelet/log"
	"github.com/drewstinnett/gout/v2"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/enso-wabisabi/pkg/enso"
	"gitlab.oit.duke.edu/devil-ops/enso-wabisabi/pkg/horizon"

	"github.com/spf13/viper"
)

var (
	cfgFile string
	c       *enso.Client
	verbose bool
	outter  *gout.Gout
	started time.Time
	version = "dev"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:           "enso-wabisabi",
	Short:         "Interact with Enso API",
	Long:          `Interact with Enso API.`, // Uncomment the following line if your bare application
	Version:       version,
	SilenceUsage:  true,
	SilenceErrors: true,
	PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
		started = time.Now()
		// viper.BindPFlag("long-lived-token", cmd.PersistentFlags().Lookup("long-lived-token"))
		var err error
		// Get output thing
		outter, err = gout.NewWithCobraCmd(cmd, &gout.CobraConfig{
			FormatField: "output-format",
		})
		if err != nil {
			return err
		}

		splay, err := cmd.Flags().GetDuration("splay")
		if err != nil {
			return err
		}

		// Secrets out of a file if it is set
		secretsF := mustGetCmd[string](*cmd, "secrets-file")
		if secretsF != "" {
			if err := enhanceViper(viper.GetViper(), secretsF); err != nil {
				return err
			}
		}

		token := viper.GetString("enso-token")
		if token == "" {
			slog.Warn("No ENSO_TOKEN set. This is ok if we are just hitting the traditional CMDB, but that will go away at some point in the future.")
		}
		ensoConfig := enso.ClientConfig{
			Token: token,
		}
		c = enso.NewClient(ensoConfig, nil)

		// Splay if asked
		if splay > time.Duration(0) {
			r := rand.New(rand.NewSource(time.Now().UnixNano())) // nolint:gosec // Don't care about insecure random waits
			rd := time.Second * time.Duration(r.Intn(int(splay.Seconds())))
			slog.Info("sleeping before running", "splay", rd.String())
			time.Sleep(rd)
		}
		return nil
	},
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.ensowabisabi.yaml)")
	rootCmd.PersistentFlags().String("cmdb-token", "", "Token to use for CMDB authentication")
	rootCmd.PersistentFlags().String("enso-token", "", "Token to use for Enso authentication")
	rootCmd.PersistentFlags().String("cartographer-token", "", "Token to use for Cartographer authentication")
	rootCmd.PersistentFlags().String("cartographer-user", "", "User to use for Cartographer authentication")
	rootCmd.PersistentFlags().String("output-format", "yaml", "Format of output where applicable")
	rootCmd.PersistentFlags().String("secrets-file", "", "File containing secrets to use. This file can be referensed to secret files using the format defined at https://github.com/variantdev/vals")
	rootCmd.PersistentFlags().Duration("splay", time.Duration(0), "Wait a random amount of time up to this interval before contacting the API")
	rootCmd.PersistentFlags().Bool("curl", false, "Print curl commands for all the web calls where we can")

	horizon.BindCobra(rootCmd)

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Verbose Logging")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	initLogging()
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		panicIfErr(err)

		// Search config in home directory with name ".cli" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".enso-wabisabi")
	}
	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	if err := viper.BindPFlag("enso-token", rootCmd.Flags().Lookup("enso-token")); err != nil {
		panic(err)
	}
	if err := viper.BindPFlag("cmdb-token", rootCmd.Flags().Lookup("cmdb-token")); err != nil {
		panic(err)
	}
	if err := viper.BindPFlag("cartographer-token", rootCmd.Flags().Lookup("cartographer-token")); err != nil {
		panic(err)
	}
	if err := viper.BindPFlag("cartographer-user", rootCmd.Flags().Lookup("cartographer-user")); err != nil {
		panic(err)
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		slog.Debug("using config file", "file", viper.ConfigFileUsed())
	}
}

func initLogging() {
	lopts := log.Options{
		TimeFormat: time.StampMilli,
		Prefix:     "enso-wabisabi 🥀 ",
	}
	if verbose {
		lopts.Level = log.DebugLevel
	}
	slog.SetDefault(slog.New(log.NewWithOptions(
		os.Stderr,
		lopts,
	)))
}
