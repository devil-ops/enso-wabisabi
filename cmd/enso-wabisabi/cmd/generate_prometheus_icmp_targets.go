package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
)

// generatePrometheusICMPTargetsCmd represents the generatePrometheusICMPTargets command
var generatePrometheusICMPTargetsCmd = &cobra.Command{
	Use:   "prometheus-icmp-targets",
	Args:  cobra.ExactArgs(0),
	Short: "Generate icmp target list",
	RunE: func(cmd *cobra.Command, _ []string) error {
		devices, _, err := c.Host.ICMP(context.Background())
		if err != nil {
			return fmt.Errorf("error getting ICMP targets: %w", err)
		}
		return marshalToFile(devices.Targets(), mustGetCmd[string](*cmd, "output-file"))
	},
}

func init() {
	generateCmd.AddCommand(generatePrometheusICMPTargetsCmd)
	generatePrometheusICMPTargetsCmd.Flags().String("output-file", "", "Output to the given filepath instead of stdout")
	/*
		if err := bindJobCobra(generatePrometheusICMPTargetsCmd); err != nil {
			panic(err)
		}
	*/
}
